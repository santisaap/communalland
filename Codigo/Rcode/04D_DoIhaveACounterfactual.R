load("CreatedData/tierras_comunales_fixed.RData")
shapetcs_single=gUnaryUnion(shapetcs)


load("CreatedData/NearFronteraPoints_Georeference.RData")
Controls=CONTROLS_NEW
pts=Controls[,c("lon_near","lat_near")]
coordinates(pts)=~lon_near+lat_near
proj4string(pts)=CRS("+init=epsg:4326")
pts = spTransform(pts,CRS_usar)
Controls$x_utm_near=pts@coords[,1]
Controls$y_utm_near=pts@coords[,2]


Controls$ReflectionX=2*Controls$x_utm_near-Controls$x_utm
Controls$ReflectionY=2*Controls$y_utm_near-Controls$y_utm

 plot(Controls[1,c("x_utm","y_utm")],type="p",pch=1,xlim=c(267810.0-100,267810.0+100),ylim=c(418508.3-100,418508.3+100))
 points(Controls[1,c("x_utm_near","y_utm_near")],pch=2)
 points(Controls[1,c("ReflectionX","ReflectionY")],pch=3)

Tolerancia=90
NuevaBase=NULL
#
for(i in unique(Controls$id_tc)){
  tf=Sys.time()
  INDEXt=which(Controls$id_tc==i)
  Controls_t=Controls[INDEXt,]
  IndexAndentro=which(Controls_t$distance<=0)
  IndexAfuera=which(Controls_t$distance>0)
  Controls_tAdentro=Controls_t[IndexAndentro,]
  Controls_tAfuera=Controls_t[IndexAfuera,]

  Yref_Adentro=Controls_tAdentro$ReflectionY
  Xref_Adentro=Controls_tAdentro$ReflectionX
  

  Yref_Afuera=Controls_tAfuera$ReflectionY
  Xref_Afuera=Controls_tAfuera$ReflectionX
  shapetcs_id=shapetcs[shapetcs$id_tc==i,]
  if(length(Xref_Adentro)>0){
    LogicAdentro=gIntersects(SpatialPoints(data.frame(Xref_Adentro,Yref_Adentro),CRS_usar),shapetcs_single,byid=T)
    NuevaBase=c(NuevaBase,INDEXt[IndexAndentro[which(LogicAdentro[1,]==F)]])
  }
  if(length(Xref_Afuera)>0){
    LogicAfuera=gIntersects(SpatialPoints(data.frame(Xref_Afuera,Yref_Afuera),CRS_usar),shapetcs_id,byid=T)
    NuevaBase=c(NuevaBase,INDEXt[IndexAfuera[which(LogicAfuera[1,]==T)]])
  } 
 
 
  
  print(tf-Sys.time())
}

ControlsContrafact=Controls[NuevaBase,]


write.dta(ControlsContrafact,file=paste0(pathtemp,"/controlDF_r3_CF.dta",sep=""))

