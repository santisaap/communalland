library(sp)
library(raster)



datadf<-read.dta(file=paste0(pathtemp,"/controlDF_r3_CF.dta"))
datadf$inReg<-1
coordinates(datadf) <- ~x_geo+y_geo 
datadf@proj4string<-CRS("+init=epsg:4326")
datadf<-spTransform(datadf,CRS_usar)

for (box in c(1:6)) {
  if (!file.exists(paste0(pathtemp,'/rasterinReg',box,'_r3.tif'))) {
    rs <- raster(paste0(pathtemp2,'/Hansen/lossyear/Hansen_GFC-2016-v1.4_lossyear_10N_080W_cropped_rxr_box_',box,'.tif'))  # this will be the template
    rs[] <- NA  # assigns all values as NA
    
    r <- rasterize(datadf,rs, datadf$inReg,fun=max)
    writeRaster(r,paste0(pathtemp,'/rasterinReg',box,'_r3.tif'),overwrite=TRUE)
    
  }
}

