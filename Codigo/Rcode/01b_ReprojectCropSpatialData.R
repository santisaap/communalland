###This code is very slow and should ONLY be run once!!!
radius=5000
#Pretty slow, only do once!
##COMUNAL LANDS
## Need to reproject and fix geometry
shapetcs<-readOGR(dsn="RawData/SIGOT/Tierras de Comunidades Negras (2016)",layer='Tierras de Comunidades Negras (2016) ')
shapetcs <- spTransform(shapetcs, CRS_usar)
#Eliminar zonas por fuera del area
load("CreatedData/ZonaLeyPorCuencas/ZonaLeyPorCuencas.RData")
A=gIntersects(shapetcs, Zona, byid = T)
shapetcs=shapetcs[which(A==T),]

shapetcs <- clgeo_Clean(shapetcs)
report <- clgeo_CollectionReport(shapetcs)
summary <- clgeo_SummaryReport(report)
issues <- report[report$valid == FALSE,]
nv <- clgeo_SuspiciousFeatures(report)
if(!is.na(nv) & nv!=0) shapetcs=shapetcs[-nv,]

shapetcs@data[,dim(shapetcs@data)[2]+1]=c(1:nrow(shapetcs@data))
colnames(shapetcs@data)[dim(shapetcs@data)[2]]<-"id_tc"
shapetcs$areacalc_sqkm=gArea(shapetcs,byid=T)
writeOGR(obj=shapetcs,dsn="CreatedData/tierras_comunales_fixed",layer='tierras_comunales_fixed',driver="ESRI Shapefile",overwrite_layer=T)
save(shapetcs,file="CreatedData/tierras_comunales_fixed.RData")

shapebufftcs<-gBuffer(shaperawtcs, width=radius,byid=T)
writeOGR(obj=shapebufftcs,dsn=paste0(pathout,'/tierras_comunales_fixed'),layer='tierras_comunales_fixed_buffer',driver="ESRI Shapefile",overwrite_layer=T)

pathRastersIn<-file.path(mainPath, 'RawData')
pathRastersOut<-file.path(mainPath, 'CreatedData','Reprojected_Cropped')


outfile <- file.path(mainPath,'temporary', paste0('reproject_Hansen_log.txt'))



image_dirs <- dir(pathRastersIn,
                  pattern=".+\\.tif$",
                  full.names=F,recursive=T)

image_dirs2 <- dir(pathRastersIn,
                  pattern=".+\\.nc$",
                  full.names=F,recursive=T)

load("CreatedData/CropExtent.RData")

for(x in image_dirs){
  if(!file.exists(paste0(pathRastersOut,"/",gsub("\\.tif","_cropped\\.tif",x)))){
    dir.create(paste0(pathRastersOut,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")),showWarnings=F,recursive=T)
    
    r<-raster(paste0(pathRastersIn,"/",x))
    
    gdalwarp(srcfile=paste0(pathRastersIn,"/",x),dstfile=paste0(pathRastersOut,"/",x), 
             t_srs='+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0', 
             multi=TRUE,output_Raster=TRUE,
             overwrite=TRUE,verbose=TRUE)
    
    gdal_translate(src_dataset=paste0(pathRastersOut,"/",x),dst_dataset=paste0(pathRastersOut,"/",gsub("\\.tif","_cropped\\.tif",x)), 
                   projwin=c(xmin(CropExtent), ymax(CropExtent), xmax(CropExtent), ymin(CropExtent)), 
             output_Raster=TRUE,
             overwrite=TRUE,verbose=TRUE,a_nodata=NA)
    
    file.remove(paste0(pathRastersOut,"/",x))
    gc()
  }
  
}


for(x in image_dirs2){
  if(!file.exists(paste0(pathRastersOut,"/",gsub("\\.nc","_cropped\\.tif",x)))){
    dir.create(paste0(pathRastersOut,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")),showWarnings=F,recursive=T)
    
    r<-raster(paste0(pathRastersIn,"/",x))
    writeRaster(r,file=gsub(".nc",".tiff",paste0(pathRastersIn,"/",x)),overwrite=TRUE)
    x=gsub(".nc",".tiff",x)
    gdalwarp(srcfile=paste0(pathRastersIn,"/",x),dstfile=paste0(pathRastersOut,"/",x), 
             t_srs='+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0', 
             multi=TRUE,output_Raster=TRUE,
             overwrite=TRUE,verbose=TRUE)
    gdal_translate(src_dataset=paste0(pathRastersOut,"/",x),dst_dataset=paste0(pathRastersOut,"/",gsub("\\.tif","_cropped\\.tif",x)), 
                   projwin=c(xmin(CropExtent), ymax(CropExtent), xmax(CropExtent), ymin(CropExtent)), 
                   output_Raster=TRUE,
                   overwrite=TRUE,verbose=TRUE,a_nodata=NA)
    file.remove(paste0(pathRastersOut,"/",x))
    file.remove(x)
    gc()
  }
  
}

image_dirs3 <- dir(pathRastersIn,
                   pattern=".+\\.adf",
                   full.names=F,recursive=T)

image_dirs3=image_dirs3[grepl("w001001\\.adf",image_dirs3)]
image_dirs3=gsub("/w001001\\.adf","",image_dirs3)


for(x in image_dirs3){
  if(!file.exists(paste0(pathRastersOut,"/",x,"pop_cropped.tif"))){
    dir.create(paste0(pathRastersOut,"/",x),showWarnings=F,recursive=T)


    xDriver <- new("GDALReadOnlyDataset", paste0(pathRastersIn,"/",x))
    getDriver(xDriver)
    getDriverLongName(getDriver(xDriver))
    xx<-asSGDF_GROD(xDriver)
    r <- raster(xx)
    writeRaster(r,file=paste0(pathRastersIn,"/",x,"pop.tif"),overwrite=TRUE)

    x=paste0(x,"pop.tif")
    gdalwarp(srcfile=paste0(pathRastersIn,"/",x),dstfile=paste0(pathRastersOut,"/",x),
             t_srs='+proj=utm +zone=18 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0',
             multi=TRUE,output_Raster=TRUE,
             overwrite=TRUE,verbose=TRUE)
    gdal_translate(src_dataset=paste0(pathRastersOut,"/",x),dst_dataset=paste0(pathRastersOut,"/",gsub("\\.tif","_cropped\\.tif",x)),
                   projwin=c(xmin(CropExtent), ymax(CropExtent), xmax(CropExtent), ymin(CropExtent)),
                   output_Raster=TRUE,
                   overwrite=TRUE,verbose=TRUE,a_nodata=NA)
    file.remove(paste0(pathRastersOut,"/",x))
    file.remove(paste0(pathRastersIn,"/",x))
    gc()
  }

}

image_dirs4 <- dir(pathRastersIn,
                   pattern=".+\\.shp$",
                   full.names=F,recursive=T)

for(x in image_dirs4){
  if(!file.exists(paste0(pathRastersOut,"/",gsub("\\.shp","_cropped\\.shp",x)))){
    dir.create(paste0(pathRastersOut,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")),showWarnings=F,recursive=T)
    if(grepl("DSMW",x) | grepl("groads-v1-americas",x) | grepl("mercado",x) | grepl("catastrales",x) | grepl("frontera_agricola",x)){
      next
    }
    
    shape=readOGR(dsn=paste0(pathRastersIn,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")), 
                  layer=gsub("\\.shp","",str_split(x, "/")[[1]][length(str_split(x, "/")[[1]])]))
    
 
    shape <- spTransform(shape,CRS_usar)
    shape <- crop(shape,CropExtent)
    
   
    writeOGR(obj=shape,dsn=paste0(pathRastersOut,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")), 
                  layer=gsub("\\.shp","_cropped",str_split(x, "/")[[1]][length(str_split(x, "/")[[1]])]),
                  driver="ESRI Shapefile",overwrite_layer=T)
    gc()
  }
  
}

#### stil need to figure out these three and how to do them fast

for(x in image_dirs4){
  if(!file.exists(paste0(pathRastersOut,"/",gsub("\\.shp","_cropped\\.shp",x)))){
    dir.create(paste0(pathRastersOut,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")),showWarnings=F,recursive=T)
    if(grepl("mercado",x) | grepl("catastrales",x) | grepl("frontera_agricola",x)){

      proj4string(yourshapefile.pr)

      r.test <- raster(CropExtent, resolution = 30)
      projection(r.test) <- CRS("+init=epsg:4686")
      writeRaster(r.test, filename=paste0(pathRastersOut,"/","text.tif"), overwrite=TRUE)


      folder=paste0(pathRastersIn,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/"))
      lay=gsub("\\.shp","",str_split(x, "/")[[1]][length(str_split(x, "/")[[1]])])
      gdal_rasterize(src_datasource=paste0(pathRastersIn,"/",x),
                     dst_filename=paste0(pathRastersOut,"/","text.tif"),
                     b=1,burn=1,l=lay,
                     output_Raster = T, verbose = T)

      shape=readOGR(dsn=paste0(pathRastersIn,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")),
                    layer=gsub("\\.shp","",str_split(x, "/")[[1]][length(str_split(x, "/")[[1]])]))

      CropExtent_p <- as(CropExtent, 'SpatialPolygons')
      proj4string(CropExtent_p)=CRS_usar
      CropExtent_p <- spTransform(CropExtent_p,proj4string(shape))
      Test=gIntersects(shape, CropExtent_p , byid = T)
      shape=shape[which(Test[1,]),]
      shape=gSimplify(shape,tol=0.01)
      shape <- spTransform(shape,CRS_usar)
      shape <- crop(shape,CropExtent)


      writeOGR(obj=shape,dsn=paste0(pathRastersOut,"/",paste0(str_split(x, "/")[[1]][-length(str_split(x, "/")[[1]])],collapse="/")),
               layer=gsub("\\.shp","_cropped",str_split(x, "/")[[1]][length(str_split(x, "/")[[1]])]),
               driver="ESRI Shapefile",overwrite_layer=T)
      gc()
    }
  }

}


#The elevations ones, I'm gonna do a mosaic!!

image_dirs <- dir('CreatedData/Reprojected_Cropped/Elevation_ALOS/N000W080_N005W075/MEDIAN',
                  pattern=".+\\.tif$",
                  full.names=T,recursive=T)
image_dirs=image_dirs[grepl("DSM",image_dirs)]

mosaic_rasters(gdalfile=image_dirs, 
               dst_dataset='CreatedData/Reprojected_Cropped/Elevation_ALOS/N000W080_N005W075/Median.tif', 
               output_Raster = T)



image_dirs <- dir('CreatedData/Reprojected_Cropped/Elevation_ALOS/N005W080_N010W075/MEDIAN',
                  pattern=".+\\.tif$",
                  full.names=T,recursive=T)
image_dirs=image_dirs[grepl("DSM",image_dirs)]

mosaic_rasters(gdalfile=image_dirs, 
               dst_dataset='CreatedData/Reprojected_Cropped/Elevation_ALOS/N005W080_N010W075/Median.tif', 
               output_Raster = T)


mosaic_rasters(gdalfile=c('CreatedData/Reprojected_Cropped/Elevation_ALOS/N000W080_N005W075/Median.tif',
                          'CreatedData/Reprojected_Cropped/Elevation_ALOS/N005W080_N010W075/Median.tif'), 
               dst_dataset='CreatedData/Reprojected_Cropped/Elevation_ALOS/Median.tif', 
               output_Raster = T)

 file.remove(c('CreatedData/Reprojected_Cropped/Elevation_ALOS/N000W080_N005W075/Median.tif','CreatedData/Reprojected_Cropped/Elevation_ALOS/N005W080_N010W075/Median.tif'))


gdal_translate(src_dataset='CreatedData/Reprojected_Cropped/Elevation_ALOS/Median.tif',
               dst_dataset='CreatedData/Reprojected_Cropped/Elevation_ALOS/Median_cropped.tif', 
               projwin=c(xmin(CropExtent), ymax(CropExtent), xmax(CropExtent), ymin(CropExtent)), 
               output_Raster=TRUE,
               overwrite=TRUE,verbose=TRUE)

file.remove(c('CreatedData/Reprojected_Cropped/Elevation_ALOS/Median.tif'))
