
dist_plot=2000
Controls=read.dta("CreatedData/Temporary/controlDF_r3.dta")
Controls=Controls[which(abs(Controls$distance)<dist_plot),]
Controls=Controls[which(!(Controls$dis_out>0 & Controls$distance>0)),]
Controls$diff=abs(abs(Controls$distance)-abs(Controls$dis_out))
Controls=Controls[which(!(Controls$diff>90 & Controls$distance<=0)),]

idMax=aggregate(Controls$distance,FUN=max,by=list(Controls$id_tc))
idMin=aggregate(Controls$distance,FUN=min,by=list(Controls$id_tc))
idDist=merge(idMax,idMin,by="Group.1")
idDist$maxDist=pmin(abs(idDist$x.x),abs(idDist$x.y))
idDist=idDist[,c(1,4)]
colnames(idDist)=c("id_tc","maxDist")
Controls=merge(Controls,idDist,by="id_tc",all=T)
Controls=Controls[which(abs(Controls$distance)<=Controls$maxDist),]
Controls=Controls[,c("id_tc","distance","dis_linea","dis_out","x","y")]
coordinates(Controls)=~x+y
proj4string(Controls)=CRS("+init=epsg:4326")
Controls@data$x_geo=Controls@coords[,1]
Controls@data$y_geo=Controls@coords[,2]
Controls = spTransform(Controls,CRS_usar)
Controls@data$x_utm=Controls@coords[,1]
Controls@data$y_utm=Controls@coords[,2]

load("CreatedData/tierras_comunales_fixed.RData")
shapetcs_bordes=gBuffer(shapetcs,width=-5,byid=T)
shapetcs_bordes=as(shapetcs,"SpatialLinesDataFrame")





 Controls = spTransform(Controls,CRS("+init=epsg:4326"))
 shapetcs_bordes = spTransform(shapetcs_bordes,CRS("+init=epsg:4326"))
 CONTROLS_NEW=NULL
 for(tc in unique(Controls$id_tc)){
   tf=Sys.time()
   Controls_t=Controls[Controls$id_tc==tc,]
   line_t=gSimplify(shapetcs_bordes[shapetcs_bordes$id_tc==tc,],tol=0.001)
   dist.df=geosphere::dist2Line(p =Controls_t , line =line_t )
   colnames(dist.df) <- c("distance_nearpoint", "lon_near", "lat_near", "segnum")
   Controls_t=cbind(Controls_t,dist.df)
   CONTROLS_NEW=rbind(CONTROLS_NEW,Controls_t@data)
   print(tf-Sys.time())
 }
 save(CONTROLS_NEW,file="CreatedData/NearFronteraPoints_Georeference.RData")
 