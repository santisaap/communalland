
#Functions
#Own deforestation
G<-function(f) {return(f^aG)}
#Forest valuation
H<-function(f) {return(bH*f^aH)}
#Effort cost
C<-function(f,e) {return(bC*(f+e*be)^aC)}
#Utility

#Functions Prime
#Own deforestation
G_prime<-function(f) {return(aG*f^(aG-1))}
#Forest valuation
H_prime<-function(f) {return(bH*aH*f^(aH-1))}
#Effort cost
C_prime_f<-function(f,e) {return(bC*aC*(f+e*be)^(aC-1))}
C_prime_e<-function(f,e) {return(be*bC*aC*(f+e*be)^(aC-1))}

#Write first order conditions
FunctionResolver=function(x,parms){
  f_1=x[1]
  f_2=x[2]
  e=x[3]
  F0=parms[1]
  N1=parms[2]
  Nmax=parms[3]
  return(c(G_prime(f_1)-C_prime_f(f_1,e)-H_prime(F0-N1*f_1-(f_2/(1+N1*e))*(Nmax-N1)),
           (f_2*(Nmax-N1)/((1+N1*e)^2))*H_prime(F0-N1*f_1-(f_2/(1+N1*e))*(Nmax-N1))-C_prime_e(f_1,e),
           (1/(1+N1*e))*G_prime(f_2/(1+N1*e))-C_prime_f(f_2,0)))
}

FunctionResolver_comunes=function(x,parms){
  F0=parms[1]
  N1=parms[2]
  Nmax=parms[3]
  return(G_prime(x)-C_prime_f(x,0)-H_prime(F0-Nmax*x))
}

FunctionResolver_optimo=function(x,parms){
  F0=parms[1]
  N1=parms[2]
  Nmax=parms[3]
  return(G_prime(x)-C_prime_f(x,0)-Nmax*H_prime(F0-Nmax*x))
}
###Parameters
#Own deforestation
aG=0.5
#forest valuation
aH=0.5

#Cost of effort
aC=2
bC=1
be=1
#number of people
Nmax=2000
#size of the forest
#F0=545000
Fvec=seq(400000,732000,4000)
N1vec=seq(0.09*Nmax,0.92*Nmax,0.01*Nmax)
HMmat=matrix(data=NA,nrow=length(Fvec),ncol=length(N1vec))
HMdefostar=matrix(data=NA,nrow=length(Fvec),ncol=length(N1vec))
HMdefostar_comunes=matrix(data=NA,nrow=length(Fvec),ncol=length(N1vec))
#We usually have 3 levels of forest
bHvec=seq(3,3,1)
f1starvec=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))
f2starvec=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))
estarvec=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))
defostarvec=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))


defostarvec_comunes=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))
f1starvec_comunes=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))
defostarvec_optimo=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))
f1starvec_optimo=matrix(data=NA,nrow=length(bHvec),ncol=length(N1vec))

init=c(0.1,0.1,0.1)
#Loop over Forest size
for (iF in 1:length(Fvec)) {
  F0=Fvec[iF]
  #Loop over forest valuation
  for (ibH in 1:length(bHvec)) {
    bH=bHvec[ibH]
    #  Loop over N owner communal land
    for (iN1 in 1:length(N1vec)) {
      N1=N1vec[iN1]
      #Solve first order conditions
      Sol=multiroot(FunctionResolver, start=c(0.1,0.1,0.1),positive=T,maxiter=1000,parms=c(F0,N1,Nmax))
      f1starvec[ibH,iN1]=Sol$root[1]
      f2starvec[ibH,iN1]=Sol$root[2]
      estarvec[ibH,iN1]=Sol$root[3]
      init=Sol$root
      defostarvec[ibH,iN1]=f1starvec[ibH,iN1]*N1+f2starvec[ibH,iN1]*(Nmax-N1)
      
      
      Sol=uniroot(FunctionResolver_comunes, interval=c(0.01,1),maxiter=1000,parms=c(F0,N1,Nmax))
      f1starvec_comunes[ibH,iN1]=Sol$root
      defostarvec_comunes[ibH,iN1]=f1starvec_comunes[ibH,iN1]*Nmax
      Sol=uniroot(FunctionResolver_optimo, interval=c(0.01,1),maxiter=1000,parms=c(F0,N1,Nmax))
      f1starvec_optimo[ibH,iN1]=Sol$root
      defostarvec_optimo[ibH,iN1]=f1starvec_optimo[ibH,iN1]*Nmax
    }
  }
  
  HMdefostar[iF,]=defostarvec[length(bHvec),]
  HMdefostar_comunes[iF,]=defostarvec_comunes[length(bHvec),]
} #end forest size loop

save(HMdefostar,HMdefostar_comunes,file="CreatedData/modelo_heatmap.RData")

#HMAT=Filas es forest size, y columnas son population size
load("CreatedData/modelo_heatmap.RData")
HMmat=100*(HMdefostar/HMdefostar_comunes-1)
col <- rev(colorRampPalette(brewer.pal(10, "RdYlGn"))(256))





rownames(HMmat)=Fvec/10000
colnames(HMmat)=N1vec
x.scale <- list(at=seq(1,length(Fvec),6),labels=Fvec[seq(1,length(Fvec),6)]/10000)
y.scale <- list(at=seq(1,length(N1vec),6), label=N1vec[seq(1,length(N1vec),6)])

temp=Fvec[seq(1,length(Fvec),6)]/10000
lm(seq(1,length(N1vec),6)~temp)
lm(seq(1,length(N1vec),6)~N1vec[seq(1,length(N1vec),6)])


library(readstata13)
Tierras=read.dta13(paste0(mainPath,"/CreatedData/Temporary/tierras_comunales_fixed_light.dta"))
Tierras=Tierras[,c("area_sqkm","habitantes")]
Tierras=Tierras[complete.cases(Tierras),]
Tierras$area_sqkm=Tierras$area_sqkm/100 #(m^2 *10000)
#Tierras$area_sqkm=-99+2.5*(Tierras$area_sqkm)
#Tierras$habitantes=-8+0.05*Tierras$habitantes
coordinates(Tierras) <- ~area_sqkm+habitantes


pdf(paste0(gitpath,"paper/figures/HeatPlot.pdf"))
levelplot(HMmat ,xlab="Forest Cover (x 10,000)", ylab="Population",
          main="",col.regions=col,scales=list(x=x.scale, y=y.scale),
          at=seq(-10,10,2),cex.axis=2)
sp.points(Tierras, pch=19, cex=1, col=1)


dev.off()

#Plot Total deforestation
pdf(paste0(graph_path,'Totaldef_N',Nmax,'F',F0,'.pdf'))
plot(N1vec,defostarvec[1,],ylim=range(defostarvec[1,],defostarvec[2,],defostarvec[3,]),col="#F15854",type="l",lwd=3,xlab="N titled",ylab="Total deforestation",cex.lab=1.5)
lines(N1vec,defostarvec[2,],col="#5DA5DA",lwd=3,lty=5)
lines(N1vec,defostarvec[3,],col="forestgreen",lwd=3,lty=3)

legend("topleft",legend=c("Low bH","Medium bH","High bH"),col=c("#F15854","#5DA5DA","forestgreen"),lty=c(1,5,3),lwd=3,bty="n")
dev.off()


pdf(paste0(graph_path,'NetEffectdef_N',Nmax,'F',F0,'.pdf'))
plot(N1vec,defostarvec[1,]-defostarvec_comunes[1,],ylim=range(defostarvec[1,]-defostarvec_comunes[1,],defostarvec[2,]-defostarvec_comunes[2,],defostarvec[3,]-defostarvec_comunes[3,]),col="#F15854",type="l",lwd=3,xlab="N titled",ylab="Total deforestation",cex.lab=1.5)
lines(N1vec,defostarvec[2,]-defostarvec_comunes[2,],col="#5DA5DA",lwd=3,lty=5)
lines(N1vec,defostarvec[3,]-defostarvec_comunes[3,],col="forestgreen",lwd=3,lty=3)
legend("topleft",legend=c("Low bH","Medium bH","High bH"),col=c("#F15854","#5DA5DA","forestgreen"),lty=c(1,5,3),lwd=3,bty="n")
dev.off()

#Plot Individual effort
png(paste0(graph_path,'Indive_N',Nmax,'F',F0,'.png'))
plot(N1vec,estarvec[1,],ylim=range(estarvec[1,],estarvec[2,],estarvec[3,]),col="#F15854",type="l",lwd=3,xlab="N titled",ylab="Individual monitoring effort",cex.lab=1.5)
lines(N1vec,estarvec[2,],col="#5DA5DA",lwd=3,lty=5)
lines(N1vec,estarvec[3,],col="forestgreen",lwd=3,lty=3)
legend("topright",legend=c("Low bH","Medium bH","High bH"),col=c("#F15854","#5DA5DA","forestgreen"),lty=c(1,5,3),lwd=3,bty="n")
dev.off()

#Plot Total effort
png(paste0(graph_path,'Totale_N',Nmax,'F',F0,'.png'))
plot(N1vec,N1vec*estarvec[1,],ylim=range(N1vec*estarvec[1,],N1vec*estarvec[2,],N1vec*estarvec[3,]),col="#F15854",type="l",lwd=3,xlab="N titled",ylab="Total monitoring effort",cex.lab=1.5)
lines(N1vec,N1vec*estarvec[2,],col="#5DA5DA",lwd=3,lty=5)
lines(N1vec,N1vec*estarvec[3,],col="forestgreen",lwd=3,lty=3)
legend("topright",legend=c("Low bH","Medium bH","High bH"),col=c("#F15854","#5DA5DA","forestgreen"),lty=c(1,5,3),lwd=3,bty="n")
dev.off()

#Plot Individual deforestation by group
png(paste0(graph_path,'defind_bygroup_N',Nmax,'F',F0,'.png'))
plot(N1vec,f2starvec[3,],ylim=range(f1starvec[3,],f2starvec[3,]),type="l",col="#5DA5DA",lwd=3,xlab="N titled",ylab="Individual deforestation",cex.lab=1.5)
lines(N1vec,f1starvec[3,],col="forestgreen",lwd=3,lty=3)
legend("bottomright",legend=c("Outsiders","Owners"),col=c("#5DA5DA","forestgreen"),lty=c(1,3),lwd=3,bty="n")
dev.off()

#Plot Total deforestation by group
png(paste0(graph_path,'defT_bygroup_N',Nmax,'F',F0,'.png'))
plot(N1vec,f2starvec[2,]*(Nmax-N1vec),ylim=range(f1starvec[2,]*N1vec,f2starvec[2,]*(Nmax-N1vec)),type="l",col="#5DA5DA",lwd=3,xlab="N titled",ylab="Total deforestation",cex.lab=1.5)
lines(N1vec,f1starvec[2,]*N1vec,col="forestgreen",lwd=3,lty=3)
legend("topright",legend=c("Outsiders","Owners"),col=c("#5DA5DA","forestgreen"),lty=c(1,3),lwd=3,bty="n")
dev.off()





