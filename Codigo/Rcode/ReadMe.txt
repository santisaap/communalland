00_Master - Set's the paths and so on
01_CreateLineaImaginaria -- Creates the zone where communal lands are allowed
01b_ReprojectCropSpatialData -- Reads tons of raster/shapefiles and crops them to the desired extent
01c_CreatNoComunal --- Create area no communal to make GDifference and stuff work
02_create_buffers -- I think we can delete...
02_setTemplate -- Creates the boxes, and the appropiate raster template
03A_AlignRasters_Various -- Creates several raster layers that are perfectly aligned with the setTemplate ones
03B_rivers_roads -- Does the same
04A_createcontrolDF1 -- Creates control DF
04B_ClosestPoint_Frontera ---  finds nearest point to border
04C_DoIhaveACounterfactual --- ask's whether it has a counterfactual
04D_rasterize_inReg -- Rasterized the output of 4C
04D_createpixelDF1 --- Creates databaser for regressions
04D_createcontrolDF1 --- Creates control DF again with the appropaite variable about subset

