t_col <- function(color, percent = 50, name = NULL) {
  #	  color = color name
  #	percent = % transparency
  #	   name = an optional name for the color
  ## Get RGB values for named color
  rgb.val <- col2rgb(color)
  ## Make new color using input color as base and alpha set by transparency
  t.col <- rgb(rgb.val[1], rgb.val[2], rgb.val[3],
               max = 255,
               alpha = (100-percent)*255/100,
               names = name)
  ## Save the color
  invisible(t.col)
  
}

radius=5*1000
load(paste0(mainPath, '/RawData/Municipios_GIS.RData'))
load(paste0(mainPath, '/RawData/DIVIPOLA.RData'))

Zona<-readOGR(dsn="CreatedData/ZonaLeyPorCuencas", 
              layer='ZonaLeyPorCuencas')

Delete<-readOGR(dsn="CreatedData/ZonaLeyPorCuencas", 
                layer='ZonaLeyPorCuencas_DeleteQGIS')
BordeZona=gBoundary(Zona)
Linea=gIntersection(BordeZona,Delete)
Linea <- spTransform(Linea, CRS_usar)



Municipios <- spTransform(Municipios, CRS_usar)
#lineabuff <- gBuffer(lineaimag,capStyle="ROUND", width=2*radius)

Municipios@data=merge(Municipios@data,DIVIPOLA_20130930,by="CodigoDane")

Municipios@data$DPTO_DPTO_=as.character(Municipios@data$DPTO_DPTO_)
Municipios=Municipios[as.character(Municipios@data$DPTO_DPTO_)!="88",]
# Municipios2 <- gSimplify(Municipios, tol=0.1, topologyPreserve=TRUE)
# Municipios = SpatialPolygonsDataFrame(Municipios2, data=Municipios@data)
Departamentos <- gUnaryUnion(Municipios, id = Municipios@data$DPTO_DPTO_)


Departamentos <- gSimplify(Departamentos, tol=0.1, topologyPreserve=TRUE)

lu <- as.data.frame(capitalize(unique(Municipios@data$Departamento)))
colnames(lu) <- "Departamento"
row.names(Departamentos) <- as.character(1:length(Departamentos))
Departamentos= SpatialPolygonsDataFrame(Departamentos, data=lu)

Pacifico=Municipios[
  Municipios@data$DPTO_DPTO_=="27" | #choco
    
    Municipios@data$DPTO_DPTO_=="52"  #Narino
  
  #Municipios@data$DPTO_DPTO_=="66" #Risaralda
  ,]
lu <- as.data.frame(capitalize(unique(Pacifico@data$Departamento)))
colnames(lu) <- "Departamento"
Pacifico <- gUnaryUnion(Pacifico, id = Pacifico@data$DPTO_DPTO_)
Pacifico <- gSimplify(Pacifico, tol=0.1, topologyPreserve=TRUE)


row.names(Pacifico) <- as.character(1:length(Pacifico))
Pacifico= SpatialPolygonsDataFrame(Pacifico, data=lu)

Rasters=c("SIAC_Bosque/Colombia_Resolucion_Fina_Bosque_NoBosque_1990/Colombia_Resolucion_Fina_Bosque_NoBosque_1990.tif",
          "SIAC_Bosque/Colombia_Resolucion_Fina_Bosque_NoBosque_2000/Colombia_Resolucion_Fina_Bosque_NoBosque_2000.tif",
          "SIAC_Bosque/Colombia_Resolucion_Fina_Bosque_NoBosque_2005/Colombia_Resolucion_Fina_Bosque_NoBosque_2005.tif",
          "SIAC_Bosque/Colombia_Resolucion_Fina_Bosque_NoBosque_2010/Colombia_Resolucion_Fina_Bosque_NoBosque_2010.tif",
          "SIAC_Bosque/BNB_SMBYC_BOSQUE_NOBOSQUE_2013/BNB_SMBYC_BOSQUE_NOBOSQUE_2013.tif")

estadisticas=NULL
for(ano in c(1990,2000,2005,2010)){
  pdf(paste0(codPath,"/paper/figures/MapForest",ano,".pdf"))
  plot(Pacifico)
  Estat_box=NULL
  Pesos_box=NULL
  for(box in 1:6){
    x=Rasters[which(c(1990,2000,2005,2010)==ano)]
    r=raster(gsub("\\.tif",paste0("box_",box,"rxr_\\.tif"),paste0("CreatedData/Reprojected_Cropped/",x)))
    plot(r,add=T)

  }
  plot(Departamentos,add=T,lwd=4)
  plot(Linea,add=T,lwd=3,col=2)
  dev.off()
}




