


**** Visual RD - Mauro	
use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
keep if abs(distance)<bwtc
egen bin_distance=cut(distance), at(-2000(200)2000)
drop if missing(bin_distance)
gen anos_to_creacion=ano-ano_creatc
keep def post_tc anos_to_creacion bin_distance

collapse (mean) def, by(post_tc bin_distance)
gen bin_defobef=def if  post_tc==0
gen bin_defoaft=def if  post_tc==1
label var bin_defobef "Deforestation before communal title"
label var bin_defoaft "Deforestation after communal title"
twoway ///
scatter bin_defobef bin_distance  , graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter bin_defoaft bin_distance  , mc(blue) || ///
lfit bin_defobef bin_distance if bin_distance<0 , lc(black) || ///
lfit bin_defobef bin_distance if bin_distance>=0 , lc(black) || ///
lfit bin_defoaft bin_distance if bin_distance<0 , lc(blue) || ///
lfit bin_defoaft bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  
graph export "$latexslides/VisualRD_Distancia_Lineal.pdf", as(pdf) replace

*All borders
use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear

keep if abs(distance)<1000
egen bin_distance=cut(distance), at(-1000(200)1000)
*Stata assigns the left value of the interval so I put the value in the middle
replace bin_distance=bin_distance+100
drop if missing(bin_distance)
gen anos_to_creacion=ano-ano_creatc
keep def post_tc anos_to_creacion bin_distance

forval i=-2/2 {
preserve
drop if anos_to_creacion!=`i'
collapse (mean) def, by(post_tc bin_distance)
label var def "Deforestation"

twoway ///
scatter def bin_distance  ,    mc(black) ms(square) msize(small) || ///
scatter def bin_distance  , mc(blue) || ///
lfit def bin_distance if bin_distance<0 , lc(black) || ///
lfit def bin_distance if bin_distance>=0 , lc(black) || ///
lfit def bin_distance if bin_distance<0 , lc(blue) || ///
lfit def bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) ytitle("Deforestation") legend(off) yscale(r(2 5) noextend) graphregion(color(white)) ylabel(2[0.5]5)
graph export "$latexslides/VisualRD_Distancia_Lineal_tiempo`i'.pdf", as(pdf) replace
graph export "$latexpaper/figures/VisualRD_Distancia_Lineal_tiempo`i'.pdf", as(pdf) replace
restore
}
/*
*Linea Imaginaria
use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
*keep if abs(distance)<bwtc
*egen bin_distance=cut(distance), at(-2000(200)2000)
*keep if lineaimag==1
keep if abs(distance)<1000
egen bin_distance=cut(distance), at(-1000(100)1000)
drop if missing(bin_distance)
gen anos_to_creacion=ano-ano_creatc
keep def post_tc anos_to_creacion bin_distance

forval i=0/5 {
preserve
drop if anos_to_creacion!=`i'
collapse (mean) def, by(post_tc bin_distance)
label var def "Deforestation"

twoway ///
scatter def bin_distance  ,    mc(black) ms(square) msize(small) || ///
scatter def bin_distance  , mc(blue) || ///
lfit def bin_distance if bin_distance<0 , lc(black) || ///
lfit def bin_distance if bin_distance>=0 , lc(black) || ///
lfit def bin_distance if bin_distance<0 , lc(blue) || ///
lfit def bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) ytitle("Deforestation (mt2)") legend(off) ysc(r(0 0.2)) graphregion(color(white)) ylabel(0[0.05]0.2)
graph export "$latexslides/VisualRD_Distancia_Lineal_tiempol`i'.pdf", as(pdf) replace
restore
}
*/

use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
keep if abs(distance)<bwtc
*keep if abs(distance)<1000
gen anos_to_creacion=ano-ano_creatc
keep def post_tc anos_to_creacion distance id_tc
gen inn=(distance<0)
drop if abs(anos_to_creacion)>10
collapse (mean) def, by(anos_to_creacion inn id_tc)
reshape wide def, i(anos_to_creacion id_tc) j(inn)
gen def= def1- def0
collapse (mean) def, by(anos_to_creacion)
twoway ///
scatter def anos_to_creacion  , graphregion(color(white))  mc(black) ms(square) msize(small)



use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
keep if abs(distance)<bwtc
*keep if abs(distance)<1000
gen anos_to_creacion=ano-ano_creatc
keep def post_tc anos_to_creacion distance id_tc
gen inn=(distance<0)
drop if abs(anos_to_creacion)>10
collapse (mean) def, by(anos_to_creacion inn)
gen bin_defobef=def if  inn==0
gen bin_defoaft=def if  inn==1
label var bin_defobef "Deforestation outside the communal title"
label var bin_defoaft "Deforestation inside the communal title"

twoway ///
scatter bin_defobef anos_to_creacion  , graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter bin_defoaft anos_to_creacion  , mc(blue)  ///
xtitle("Years to titling") xline(0) legend(order(1 2 ) rows(2))  

drop bin_defobef bin_defoaft
reshape wide def, i(anos_to_creacion) j(inn)
gen def= def1- def0
twoway ///
scatter def anos_to_creacion  , graphregion(color(white))  mc(black) ms(square) msize(small)

*lfit bin_defobef anos_to_creacion if anos_to_creacion<0 , lc(black) || ///
*lfit bin_defobef anos_to_creacion if anos_to_creacion>=0 , lc(black) || ///
*lfit bin_defoaft anos_to_creacion if anos_to_creacion<0 , lc(blue) || ///
*lfit bin_defoaft anos_to_creacion if anos_to_creacion>=0 , lc(blue)

graph export "$latexslides/VisualRD_Tiempo_Lineal.pdf", as(pdf) replace
*/

**** Visual RD
/*
use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
keep if abs(distance)<bwtc
egen bin_distance=cut(distance), at(-2000(200)2000)

egen bin_defobef=mean(def) if post_tc==0 , by(bin_distance)
egen bin_defoaft=mean(def) if post_tc==1  , by(bin_distance)

label var bin_defobef "Deforestation before communal title"
label var bin_defoaft "Deforestation after communal title"

preserve
drop if missing(bin_distance)
drop if missing(bin_defobef) & missing(bin_defoaft)



twoway ///
scatter bin_defobef bin_distance  , graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter bin_defoaft bin_distance  , mc(blue) || ///
lfit bin_defobef bin_distance if distance<0 , lc(black) || ///
lfit bin_defobef bin_distance if distance>=0 , lc(black) || ///
lfit bin_defoaft bin_distance if distance<0 , lc(blue) || ///
lfit bin_defoaft bin_distance if distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  
graph export "$latexslides/VisualRD_Lineal.pdf", as(pdf) replace
restore


twoway ///
scatter bin_defobef bin_distance  , graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter bin_defoaft bin_distance  , mc(blue) || ///
qfit bin_defobef bin_distance if distance<0 , lc(black) || ///
qfit bin_defobef bin_distance if distance>=0 , lc(black) || ///
qfit bin_defoaft bin_distance if distance<0 , lc(blue) || ///
qfit bin_defoaft bin_distance if distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  


egen tag=tag(bin_distance post_tc )
twoway ///
scatter bin_defobef bin_distance if tag==1 , mc(black) ms(square) msize(small) graphregion(color(white)) || ///
scatter bin_defoaft bin_distance if tag==1 , xlabel(-1000 "-1000" 0 "0" 1000 "1000") mc(blue) graphregion(color(white)) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  

graph export "$latexslides/VisualRD.pdf", as(pdf) replace
*/



use "$mipath/CreatedData/Temporary/tierras_comunales_fixed.dta", clear

label var anos_soltocrea "Request to tilting (years)"

twoway( hist ano_creatc, bin(10) fraction color(midgreen*0.5) fcolor(midgreen*0.5%30)) (hist ano_soltc, bin(10) fraction color(red*0.5) fcolor(red*0.5%30)) , ///
        plotregion(color(white)) graphregion(color(white) lwidth(large)) ///
xtitle("Year", size(large))  ytitle("Frequency", size(large ))   title("") legend(order(2 "Requested" 1 "Granted"))
graph export "$latexpaper/figures/histReqGranted.pdf", as(pdf) replace

qui estpost tabstat  ano_creatc ano_soltc anos_soltocrea area_sqkm habitantes densidad      if id_tc!=id_tc[_n+1], statistics(mean p50 sd min max count) columns(statistics)
qui esttab using "$latexslides/sumstats_tc.tex", cells("mean(fmt(%11.2gc) label(Mean)) p50(fmt(%11.2gc) label(Median)) sd(fmt(%11.2gc) label(Std. Dev.)) min(fmt(%11.2gc) label(Min)) max(fmt(%11.2gc) label(Max)) count(fmt(%11.2gc) label(N))") nomtitle nonumber label  replace noobs  booktabs
qui esttab using "$tables/sumstats_tc.tex", cells("mean(fmt(%11.2gc) label(Mean)) p50(fmt(%11.2gc) label(Median)) sd(fmt(%11.2gc) label(Std. Dev.)) min(fmt(%11.2gc) label(Min)) max(fmt(%11.2gc) label(Max)) count(fmt(%11.2gc) label(N))") nomtitle nonumber label  replace noobs  booktabs


qui estpost tabstat  ano_creatc ano_soltc anos_soltocrea area_sqkm habitantes densidad      if id_tc!=id_tc[_n+1] & d_h1==1, statistics(mean p50 sd min max count) columns(statistics)
qui estout using "$tables/sumstats_tc_dh1_1.tex", cells("mean(fmt(%11.2gc)) p50(fmt(%11.2gc)) sd(fmt(%11.2gc) ) min(fmt(%11.2gc)) max(fmt(%11.2gc)) count(fmt(%11.2gc))")   label  replace style(tex) collabels(none) mlabels(none)
qui estpost tabstat  ano_creatc ano_soltc anos_soltocrea area_sqkm habitantes densidad      if id_tc!=id_tc[_n+1] & d_h1==0, statistics(mean p50 sd min max count) columns(statistics)
qui estout using "$tables/sumstats_tc_dh1_0.tex", cells("mean(fmt(%11.2gc)) p50(fmt(%11.2gc)) sd(fmt(%11.2gc) ) min(fmt(%11.2gc)) max(fmt(%11.2gc)) count(fmt(%11.2gc))")   label  replace style(tex) collabels(none) mlabels(none)

qui estpost tabstat  ano_creatc ano_soltc anos_soltocrea area_sqkm habitantes densidad      if id_tc!=id_tc[_n+1] & d_p1==1, statistics(mean p50 sd min max count) columns(statistics)
qui estout using "$tables/sumstats_tc_dp1_1.tex", cells("mean(fmt(%11.2gc)) p50(fmt(%11.2gc)) sd(fmt(%11.2gc) ) min(fmt(%11.2gc)) max(fmt(%11.2gc)) count(fmt(%11.2gc))")   label  replace style(tex) collabels(none) mlabels(none)
qui estpost tabstat  ano_creatc ano_soltc anos_soltocrea area_sqkm habitantes densidad      if id_tc!=id_tc[_n+1] & d_p1==0, statistics(mean p50 sd min max count) columns(statistics)
qui estout using "$tables/sumstats_tc_dp1_0.tex", cells("mean(fmt(%11.2gc)) p50(fmt(%11.2gc)) sd(fmt(%11.2gc) ) min(fmt(%11.2gc)) max(fmt(%11.2gc)) count(fmt(%11.2gc))")   label  replace style(tex) collabels(none) mlabels(none)

use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear

qui eststo clear



qui eststo m1:  reghdfe def inxpost distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local timefe "\$ h^* \$"
estadd local lawline "No"
estadd local poly "Quadratic"

matrix tempm=e(b)
local tempm=string(tempm[1,1], "%9.2fc")
local tempm_effect=abs(tempm[1,1])

file open newfile using "$tables/coef_main1.tex", write replace
file write newfile "`tempm'"
file close newfile
test inxpost
if r(p)<0.001 {
	di "peque"
	local tempm ="$<0.001$"
	file open newfile using "$tables/pvalue_main1.tex", write replace
	file write newfile "`tempm'"
	file close newfile
}
if r(p)>0.001 {
	di "grande"
	local tempm=string(r(p), "%9.2fc")
	file open newfile using "$tables/pvalue_main1.tex", write replace
	file write newfile "`tempm'"
	file close newfile
}
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m2:  reghdfe def inxpost distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers slope if abs(distance)<bwtc [aw=invarea], absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Community"

matrix tempm=e(b)
local tempm=string(tempm[1,1], "%9.2fc")
local tempm_effect=tempm[1,1]

file open newfile using "$tables/coef_main2.tex", write replace
file write newfile "`tempm'"
file close newfile
test inxpost
if r(p)<0.001 {
	di "peque"
	local tempm ="$<0.001$"
	file open newfile using "$tables/pvalue_main2.tex", write replace
	file write newfile "`tempm'"
	file close newfile
}
if r(p)>0.001 {
	di "grande"
	local tempm=string(r(p), "%9.2fc")
	file open newfile using "$tables/pvalue_main2.tex", write replace
	file write newfile "`tempm'"
	file close newfile
}
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


qui eststo m1h:  reghdfe def inxpost_p*  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


qui eststo m2h:  reghdfe def inxpost_p*  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc [aw=invarea], absorb(ano_idtc d_in##id_tc) vce(cluster ano id_tc )
qui estadd ysumm
estadd local unit "Community"
estadd local timefe "\$h^\$*"
estadd local lawline "No"
estadd local poly "Linear"

qui tab id_tc if e(sample)
estadd local Ncom=r(r)

*Terciles

gen inxpostxpopt1= inxpost*(tercile_habitantes==1) if tercile_habitantes!=.
gen inxpostxpopt2= inxpost*(tercile_habitantes==2) if tercile_habitantes!=.
gen inxpostxpopt3= inxpost*(tercile_habitantes==3) if tercile_habitantes!=.
label var inxpostxpopt3 "After X Inner X Tercile 3"
label var inxpostxpopt2 "After X Inner X Tercile 2"
label var inxpostxpopt1 "After X Inner X Tercile 1"

qui eststo m1t:  reghdfe def inxpostxpopt*  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

**Non-linear effects
gen inxpostxpop= inxpost*(habitantes/10^4)
gen inxpostxpopsq= inxpost*(habitantes/10^4)^2
label var inxpostxpop "After X Inner X Pop"
label var inxpostxpopsq "After X Inner X Pop\$^2\$"

qui eststo m1l:  reghdfe def inxpost inxpostxpop  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m1q:  reghdfe def inxpost inxpostxpop inxpostxpopsq  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


qui estout m1  m1h m1t m1l m1q  using "$tables/pixeldis_fquad_reg_tabla_main_pixel.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost  inxpost_p* inxpostxpopt* inxpostxpop inxpostxpopsq) prefoot(\midrule ) stats( N Ncom ymean r2 , fmt(  %11.2gc a2 a2 a2 ) labels ("N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui estout m1_ideam  m1h_ideam   using "$tables/pixeldis_fquad_reg_tabla_mainIDEAM_simple_mr.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost  inxpost_p* ) prefoot(\midrule ) stats( unit N Ncom ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Unit" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace



qui estout m1 m2 m1h m2h  using "$tables/pixeldis_fquad_reg_tabla_main.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost  inxpost_p*) prefoot(\midrule ) stats( unit N Ncom ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Unit" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace




qui estout m1 m2 using "$latexslides/pixeldis_wnw2k_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost  ) prefoot(\midrule ) stats( unit N N_clust ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Unit" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace


gen totitle=ano- ano_creatc
gen totitle2=totitle
replace totitle2=-5 if totitle2<=-5 & !missing(totitle2)
replace totitle2=5 if totitle2>=5 & !missing(totitle2)
replace totitle2=totitle2+5

fvset base 5 totitle2 
char totitle2[omit] 5
gen totitle2_d_in=d_in*totitle2
fvset base 5 totitle2_d_in 
char totitle2_d_in[omit] 16

reghdfe def ib5.totitle2_d_in  distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers  slope if abs(distance)<bwtc , absorb( ano_idtc d_in##id_tc) vce(cluster ano_idtc)
coefplot, graphregion(color(white)) baselevels keep(*.totitle2_d_in) ci ///
rename(0.totitle2_d_in="<=-5" 1.totitle2_d_in ="-4" 2.totitle2_d_in= "-3" ///
3.totitle2_d_in= "-2"  4.totitle2_d_in= "-1" 5.totitle2_d_in= "0" ///
6.totitle2_d_in= "1" 7.totitle2_d_in ="2" 8.totitle2_d_in= "3" ///
9.totitle2_d_in= "4" 10.totitle2_d_in =">=5") ///
yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical xline(5.5) yline(0) xtitle("Time to titling", size(large)) ///
ytitle("Deforestation coefficient", size(large))
graph export "$latexslides/figures/EventStudy_All.pdf", replace 
graph export "$latexpaper/figures/EventStudy_All.pdf", replace 

reghdfe def ib5.totitle2_d_in distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers  slope if abs(distance)<bwtc & d_p1==1, absorb( ano_idtc d_in##id_tc) vce(cluster ano_idtc)
coefplot, graphregion(color(white)) baselevels keep(*.totitle2_d_in) ci ///
rename(0.totitle2_d_in="<=-5" 1.totitle2_d_in ="-4" 2.totitle2_d_in= "-3" ///
3.totitle2_d_in= "-2"  4.totitle2_d_in= "-1" 5.totitle2_d_in= "0" ///
6.totitle2_d_in= "1" 7.totitle2_d_in ="2" 8.totitle2_d_in= "3" ///
9.totitle2_d_in= "4" 10.totitle2_d_in =">=5") ///
yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical xline(5.5) yline(0) xtitle("Time to titling", size(large)) ///
ytitle("Deforestation coefficient", size(large))
graph export "$latexslides/figures/EventStudy_Small.pdf", replace 


reghdfe def ib5.totitle2_d_in  distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers  slope if abs(distance)<bwtc & d_p1==0, absorb( ano_idtc d_in##id_tc) vce(cluster ano_idtc)
coefplot, graphregion(color(white)) baselevels keep(*.totitle2_d_in) ci ///
rename(0.totitle2_d_in="<=-5" 1.totitle2_d_in ="-4" 2.totitle2_d_in= "-3" ///
3.totitle2_d_in= "-2"  4.totitle2_d_in= "-1" 5.totitle2_d_in= "0" ///
6.totitle2_d_in= "1" 7.totitle2_d_in ="2" 8.totitle2_d_in= "3" ///
9.totitle2_d_in= "4" 10.totitle2_d_in =">=5") ///
yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical xline(5.5) yline(0) xtitle("Time to titling", size(large)) ///
ytitle("Deforestation coefficient", size(large))
graph export "$latexslides/figures/EventStudy_Big.pdf", replace 

qui eststo m1h05:  reghdfe def inxpost_p*  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers  slope if abs(distance)<(bwtc/2), absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local timefe "\$0.5h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"



qui eststo m12h:  reghdfe def inxpost_p*  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<2*bwtc, absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local timefe "\$2h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"

qui eststo m1h05l:  reghdfe def inxpost_p*  distance_in distance_out disRoads disRivers slope  if abs(distance)<(bwtc/2), absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local timefe "\$0.5h^*\$"
estadd local lawline "No"
estadd local poly "Linear"

qui eststo m1l:  reghdfe def inxpost_p*  distance_in distance_out disRoads disRivers  slope if abs(distance)<bwtc, absorb(ano_idtc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Linear"


qui eststo m12hl:  reghdfe def inxpost_p*  distance_in distance_out disRoads disRivers  slope  if abs(distance)<2*bwtc, absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local timefe "\$2h^*\$"
estadd local lawline "No"
estadd local poly "Linear"

qui estout m1h05 m1 m12h using "$latexslides/pixeldis125k_size_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost_p*  ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Bandwith" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui estout m1h05 m1h m12h m1h05l m1l m12hl using "$tables/robust_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost_p*  ) prefoot(\midrule ) stats( timefe poly N  ymean r2 , fmt( %fmt %fmt %11.2gc  a2 a2 ) labels ("Bandwith" "Polynomial" "N. of obs."  "Mean of Dep. Var." "\$R^2\$" )) replace

******************************************
***Event study with year title requested ******
******************************************
drop totitle* 

gen totitle=ano- ano_soltc
gen totitle2=totitle
replace totitle2=-5 if totitle2<=-5 & !missing(totitle2)
replace totitle2=5 if totitle2>=5 & !missing(totitle2)
replace totitle2=totitle2+5

fvset base 5 totitle2 
char totitle2[omit] 5
gen totitle2_d_in=d_in*totitle2
fvset base 5 totitle2_d_in 
char totitle2_d_in[omit] 16

reghdfe def ib5.totitle2_d_in  distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers  slope if abs(distance)<bwtc , absorb( ano_idtc d_in##id_tc) vce(cluster ano_idtc)
coefplot, graphregion(color(white)) baselevels keep(*.totitle2_d_in) ci ///
rename(0.totitle2_d_in="<=-5" 1.totitle2_d_in ="-4" 2.totitle2_d_in= "-3" ///
3.totitle2_d_in= "-2"  4.totitle2_d_in= "-1" 5.totitle2_d_in= "0" ///
6.totitle2_d_in= "1" 7.totitle2_d_in ="2" 8.totitle2_d_in= "3" ///
9.totitle2_d_in= "4" 10.totitle2_d_in =">=5") ///
yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical xline(5.5) yline(0) xtitle("Time to request", size(large)) ///
ytitle("Deforestation coefficient", size(large))
graph export "$latexslides/figures/EventStudyRequest_All.pdf", replace 
graph export "$latexpaper/figures/EventStudyRequest_All.pdf", replace 

******************************************
***Pixel Fixed EFFECTS ******
******************************************
qui eststo clear
qui eststo m1:  reghdfe def inxpost  if abs(distance)<bwtc , absorb(ano_idtc id_pixel c.disRoads##i.ano c.disRivers##i.ano c.slope##i.ano) vce(cluster ano_idtc) 
qui estadd ysumm
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m1h:  reghdfe def inxpost_p*   if abs(distance)<bwtc , absorb(ano_idtc id_pixel c.disRoads##i.ano c.disRivers##i.ano c.slope##i.ano) vce(cluster ano_idtc)
qui estadd ysumm
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

******************************************
**** IDEAM 
******************************************
gen defIDEAM=.
replace defIDEAM=defIDEAM1990 if ano==1990
replace defIDEAM=defIDEAM2000 if ano==2000
replace defIDEAM=defIDEAM2005 if ano==2005
replace defIDEAM=defIDEAM2010 if ano==2010

qui eststo m1_ideam:  reghdfe defIDEAM inxpost  distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers  slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local timefe "\$ h^* \$"
estadd local lawline "No"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m1h_ideam:  reghdfe defIDEAM inxpost_p* distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers  slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano id_tc )
qui estadd ysumm
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


qui estout m1  m1h  m1_ideam  m1h_ideam using "$tables/pixeldis_fquad_reg_tabla_main_pixel_pix.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost  inxpost_p* ) prefoot(\midrule ) stats( N Ncom ymean r2 , fmt(  %11.2gc a2 a2 a2 ) labels ("N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo m1_2wc:  reghdfe def inxpost distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano id_tc) 
qui estadd ysumm

qui eststo m1h_2wc:  reghdfe def inxpost_p* distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano id_tc) 
qui estadd ysumm


qui estout m1  m1h  m1_2wc  m1h_2wc using "$tables/pixelFE_twowayclust.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost  inxpost_p* ) prefoot(\midrule ) stats( N Ncom ymean r2 , fmt(  %11.2gc a2 a2 a2 ) labels ("N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace


******************************
** Placebo and other control
******************************

use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear

qui eststo m1:  reghdfe def inxpost distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local regt "Main"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


*Changing the control to be the ring "bwtc" out
gen distanceOc=distance if d_in==1
replace distanceOc=distance-bwtc if distance>bwtc
*No cambian
*gen d_inPl=( distancePl<=0)
*gen inPlxpost=d_inPl*post_tc
gen distanceOc_in=-distanceOc*d_in
gen distanceOc_out=distanceOc*(1-d_in)
gen distanceOc_in_sq=distanceOc_in*distanceOc_in
gen distanceOc_out_sq=distanceOc_out*distanceOc_out

qui eststo m2: reghdfe def inxpost distanceOc_in distanceOc_in_sq distanceOc_out distanceOc_out_sq disRoads disRivers slope if abs(distanceOc)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local regt "Out Control"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


*Changing the control to be the ring 2"bwtc" out
drop distanceOc*
gen distanceOc=distance if d_in==1
replace distanceOc=distance-2*bwtc if distance>2*bwtc
*No cambian
*gen d_inPl=( distancePl<=0)
*gen inPlxpost=d_inPl*post_tc
gen distanceOc_in=-distanceOc*d_in
gen distanceOc_out=distanceOc*(1-d_in)
gen distanceOc_in_sq=distanceOc_in*distanceOc_in
gen distanceOc_out_sq=distanceOc_out*distanceOc_out

qui eststo m3: reghdfe def inxpost distanceOc_in distanceOc_in_sq distanceOc_out distanceOc_out_sq disRoads disRivers slope if abs(distanceOc)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local regt "Out Control"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


*Placebo moving the boundary "bwtc" out
gen distancePl=distance-bwtc
gen d_inPl=( distancePl<=0)
gen inPlxpost=d_inPl*post_tc
label var inPlxpost "After X Placebo"
gen distancePl_in=-distancePl*d_inPl
gen distancePl_out=distancePl*(1-d_inPl)
gen distancePl_in_sq=distancePl_in*distancePl_in
gen distancePl_out_sq=distancePl_out*distancePl_out

qui eststo m4: reghdfe def inPlxpost distancePl_in distancePl_in_sq distancePl_out distancePl_out_sq disRoads disRivers slope if abs(distancePl)<bwtc , absorb(ano_idtc d_inPl##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local regt "Placebo"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

*Placebo moving the boundary 2"bwtc" out
drop distancePl* distanceOc* d_inPl inPlxpost
gen distancePl=distance-2*bwtc
gen d_inPl=( distancePl<=0)
gen inPlxpost=d_inPl*post_tc
label var inPlxpost "After X Placebo"
gen distancePl_in=-distancePl*d_inPl
gen distancePl_out=distancePl*(1-d_inPl)
gen distancePl_in_sq=distancePl_in*distancePl_in
gen distancePl_out_sq=distancePl_out*distancePl_out

qui eststo m5: reghdfe def inPlxpost distancePl_in distancePl_in_sq distancePl_out distancePl_out_sq disRoads disRivers slope if abs(distancePl)<bwtc , absorb(ano_idtc d_inPl##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local regt "Placebo"
estadd local poly "Quadratic"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)



qui estout m1  m2 m3 m4 m5  using "$tables/pixeldis_fquad_reg_PlOc.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost inPlxpost ) prefoot(\midrule ) stats( regt N Ncom ymean r2 , fmt(%fmt  %11.2gc a2 a2 a2 ) labels ("Comparison" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace




******************************************
******************************************
******************************************
/*
preserve
keep if ano==2001
keep defIDEAM* id_pixel id_tc ano_creatc d_in  invarea distance_in distance_in_sq distance_out distance_out_sq distance bwtc area_sqkm lineaimag disRoads disRivers p00agpop slope
bys id_pixel: gen n=_n
drop if n>1
reshape long defIDEAM, i(id_pixel) j(ano) 
gen post_tc=( ano>= ano_creatc) if ano_creatc!=.
gen inxpost=d_in*post_tc
gen ano_idtc=ano*1000+id_tc
sort id_tc
qui sum area_sqkm if id_tc!=id_tc[_n+1], d
qui gen d_h1=(area_sqkm<=r(p50))
qui gen inxpost_h1=inxpost*d_h1
label var inxpost "After X Inner"
label var inxpost_h1 "After X Inner X Small"
qui gen inxpost_h2=inxpost*(1-d_h1)
label var inxpost_h2 "After X Inner X Large"
qui gen d_in_h1=d_in*d_h1
label var d_in_h1 "Inner X Small"
qui gen d_in_h2=d_in*(1-d_h1)
label var d_in_h2 "Inner X Large"

qui eststo m1:  reghdfe defIDEAM inxpost d_in distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers p00agpop slope if abs(distance)<bwtc , absorb(ano_idtc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m2:  reghdfe defIDEAM inxpost d_in distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers p00agpop slope if abs(distance)<bwtc [aw=invarea], absorb(ano_idtc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Community"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m1h:  reghdfe defIDEAM inxpost_h* d_in_h* distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers p00agpop slope if abs(distance)<bwtc, absorb(ano_idtc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local unit "Pixel"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m2h:  reghdfe defIDEAM inxpost_h* d_in_h* distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers p00agpop slope if abs(distance)<bwtc [aw=invarea], absorb(ano_idtc) vce(cluster ano_idtc )
qui estadd ysumm
estadd local unit "Community"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)




qui estout m1 m2 m1h m2h  using "$tables/pixeldis_fquad_reg_tabla_mainIDEAM.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost d_in inxpost_h* d_in_h*) prefoot(\midrule ) stats( unit N Ncom ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Unit" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui estout m1  m1h   using "$tables/pixeldis_fquad_reg_tabla_mainIDEAM_simple.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost d_in inxpost_h* d_in_h*) prefoot(\midrule ) stats( unit N Ncom ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Unit" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui eststo m1_imag:  reghdfe defIDEAM inxpost d_in distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers p00agpop slope if lineaimag==1 & abs(distance)<bwtc , absorb(ano_idtc)
qui estadd ysumm
estadd local unit "Pixel"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m2_imag:  reghdfe defIDEAM inxpost d_in distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers p00agpop slope if  lineaimag==1 & abs(distance)<bwtc [aw=invarea], absorb(ano_idtc)  
qui estadd ysumm
estadd local unit "Community"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)


qui eststo m1h_imag:  reghdfe defIDEAM inxpost_h* d_in_h* distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers p00agpop slope if lineaimag==1  & abs(distance)<bwtc, absorb(ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)

qui eststo m2h_imag:  reghdfe defIDEAM inxpost_h* d_in_h* distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers p00agpop slope if lineaimag==1 &  abs(distance)<bwtc [aw=invarea], absorb(ano_idtc) 
qui estadd ysumm
estadd local unit "Community"
qui tab id_tc if e(sample)
estadd local Ncom=r(r)



qui estout  m1_imag m2_imag m1h_imag m2h_imag using "$tables/pixeldis_fquad_lineaimag_tabla_IDEAM.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost_h* d_in_h* ) prefoot(\midrule ) stats( unit N Ncom ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Unit" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace


qui estout  m1_imag  m1h_imag  using "$tables/pixeldis_fquad_lineaimag_tabla_IDEAM_pixel.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost_h* d_in_h* ) prefoot(\midrule ) stats( unit N Ncom ymean r2 , fmt( %fmt %11.2gc a2 a2 a2 ) labels ("Unit" "N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace


restore

preserve
set more off
gen coeftc=.
gen vartc=.
forval i=1/163 {
display `i'
if `i'~=25 & `i'~=26 & `i'~=30 & `i'~=55 & `i'~=65 & `i'~=66 & `i'~=152 {
*qui sum distance if d_in==1 & id_tc==`i'
qui  reghdfe def inxpost d_in distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc & id_tc==`i' , absorb(ano)
matrix tempm=e(b)
replace coeftc=tempm[1,1] in `i'
matrix tempm=e(V)
replace vartc=tempm[1,1] in `i'
}
}


keep coeftc vartc
gen id_tc=_n
keep if id_tc<164
save "$mipath/CreatedData/coef_idtc.dta", replace
gen tstattc=coeftc/sqrt(vartc)

count if coeftc!=. & vartc!=0
local temp=r(N)
file open newfile using "$latexslides/Ncoeftc.tex", write replace
file write newfile "`temp'"
file close newfile
file open newfile using "$latexpaper/Ncoeftc.tex", write replace
file write newfile "`temp'"
file close newfile

count if coeftc<0
local temp=r(N)
file open newfile using "$latexslides/Ncoeftcneg.tex", write replace
file write newfile "`temp'"
file close newfile
file open newfile using "$latexpaper/Ncoeftcneg.tex", write replace
file write newfile "`temp'"
file close newfile

count if coeftc<0 & tstat<-1.96
local temp=r(N)
file open newfile using "$latexslides/Ncoeftcnegsig.tex", write replace
file write newfile "`temp'"
file close newfile
file open newfile using "$latexpaper/Ncoeftcnegsig.tex", write replace
file write newfile "`temp'"
file close newfile

count if coeftc>0 & tstat>1.96 & coeftc!=.
local temp=r(N)
file open newfile using "$latexslides/Ncoeftcpossig.tex", write replace
file write newfile "`temp'"
file close newfile
file open newfile using "$latexpaper/Ncoeftcpossig.tex", write replace
file write newfile "`temp'"
file close newfile
restore
*coefplot (m3 \ m4) , scheme(s1color) keep(inxpost) xline(0)


*/

/*
qui eststo m1:  reghdfe def inxpost d_in , absorb(ano id_tc) vce(cluster id_tc )
qui estadd ysumm
estadd local timefe "Year and Title"

qui eststo m2:  reghdfe def inxpost d_in , absorb(ano_idtc) vce(cluster id_tc )
qui estadd ysumm
estadd local timefe "Year-Title"
qui estout m1 m2 using "$latexslides/pixel5k_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost d_in ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ("FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo m1:  reghdfe def inxpost d_in distance_in distance_out, absorb(ano id_tc) vce(cluster id_tc )
qui estadd ysumm
estadd local timefe "Year and Title"

qui eststo m2:  reghdfe def inxpost d_in distance_in distance_out, absorb(ano_idtc) vce(cluster id_tc )
qui estadd ysumm
estadd local timefe "Year-Title"
qui estout m1 m2 using "$latexslides/pixeldis5k_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost d_in ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ("FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

qui eststo m1:  reghdfe def inxpost d_in if abs(distance)<2000, absorb(ano id_tc) vce(cluster id_tc )
qui estadd ysumm
estadd local timefe "Year and Title"

qui eststo m2:  reghdfe def inxpost d_in if abs(distance)<2000, absorb(ano_idtc) vce(cluster id_tc )
qui estadd ysumm
estadd local timefe "Year-Title"
qui estout m1 m2 using "$latexslides/pixel2k_reg.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep( inxpost d_in ) prefoot(\midrule ) stats( timefe N N_clust ymean r2 , fmt( %fmt a2 a2 a2 a2 ) labels ("FE" "N. of obs." "Municipalities" "Mean of Dep. Var." "\$R^2\$" )) replace

*/
/*
*Placebo moving the boundary 3"bwtc" out
drop distancePl* d_inPl inPlxpost
gen distancePl=distance-3*bwtc
gen d_inPl=( distancePl<=0)
gen inPlxpost=d_inPl*post_tc
label var inPlxpost "After X Placebo"
gen distancePl_in=-distancePl*d_inPl
gen distancePl_out=distancePl*(1-d_inPl)
gen distancePl_in_sq=distancePl_in*distancePl_in
gen distancePl_out_sq=distancePl_out*distancePl_out

qui eststo m7: reghdfe def inPlxpost distancePl_in distancePl_in_sq distancePl_out distancePl_out_sq disRoads disRivers slope if abs(distancePl)<bwtc , absorb(ano_idtc d_inPl##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local regt "Placebo"
estadd local poly "Quadratic"

*Changing the control to be the ring 3"bwtc" out
drop distanceOc*
gen distanceOc=distance if d_in==1
replace distanceOc=distance-3*bwtc if distance>3*bwtc
*No cambian
*gen d_inPl=( distancePl<=0)
*gen inPlxpost=d_inPl*post_tc
gen distanceOc_in=-distanceOc*d_in
gen distanceOc_out=distanceOc*(1-d_in)
gen distanceOc_in_sq=distanceOc_in*distanceOc_in
gen distanceOc_out_sq=distanceOc_out*distanceOc_out

qui eststo m4: reghdfe def inxpost distanceOc_in distanceOc_in_sq distanceOc_out distanceOc_out_sq disRoads disRivers slope if abs(distanceOc)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc) 
qui estadd ysumm
estadd local unit "Pixel"
estadd local regt "Out Control"
estadd local poly "Quadratic"
