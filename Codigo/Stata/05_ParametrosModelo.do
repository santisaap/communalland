use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
drop if ano!=2000
collapse (mean) Htree2000 p00agpop area_sqkm areacalc_sqkm (count) id_pixel if abs(distance)<bwtc, by( id_tc d_in)
reshape wide Htree2000 p00agpop id_pixel, i(id_tc) j(d_in)
gen area_total=(id_pixel1+id_pixel0)*83.07*83.07/1000/1000
gen N=(areacalc_sqkm/area_total)*(p00agpop0+p00agpop1)*(83.07^2/4650^2) /*por que el origiunal de poblacion viene en resolucion 4.65x4.65 km aprox, entonce se multiplica por (90^2/4650^2)  */
gen N1=(areacalc_sqkm/area_total)*p00agpop1*(83.07^2/4650^2)
gen F=(areacalc_sqkm/area_total)*(Htree20000*id_pixel0+Htree20001*id_pixel1)*83.07*83.07/100 /*multipliying by size of pixel (90^2 mts), dividing by 100 since Htree is in %*/



export delim using "$mipath/CreatedData/parametros_modelo_idtc.csv", replace

/*
collapse (mean) Htree2000 p00agpop (count) id_pixel , by( id_tc d_in)
reshape wide Htree2000 p00agpop id_pixel, i(id_tc) j(d_in)
gen N=p00agpop0+p00agpop1
gen N1=p00agpop1
gen F=(Htree20000*id_pixel0+Htree20001*id_pixel1)*90*90/100 /*multipliying by size of pixel, dividing by 100 since Htree is in %*/
