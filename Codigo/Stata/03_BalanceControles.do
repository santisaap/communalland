capt prog drop my_ptest_sd
	*! version 1.0.0  14aug2007  Ben Jann
	program my_ptest_sd, eclass
	*clus(clus_var)
	syntax varlist [if] [in], by(varname) running(varname numeric) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

	marksample touse
	markout `touse' `by'
	tempname mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2
	capture drop TD*
	tab `by' , gen(TD)
	foreach var of local varlist {
		 reg `var'  TD1 TD2   `if', nocons vce(cluster `clus_id')
		 matrix A=e(b)
		 lincom (TD1-TD2)
		 sum `var' if TD2==1 & e(sample)==1
		 mat `mu_1' = nullmat(`mu_1'), r(mean)
		 mat `se_1' = nullmat(`se_1'), r(sd)
		 sum `var' if TD1==1 & e(sample)==1
		 mat `mu_2' = nullmat(`mu_2'),r(mean)
		 mat `se_2' = nullmat(`se_2'), r(sd)
		 
		 reg `var'  TD1 TD2   `if',  vce(cluster `clus_id')
		 test (_b[TD1]- _b[TD2]== 0)
		 mat `d_p'  = nullmat(`d_p'),r(p)
		 matrix A=e(b)
		 lincom (TD1-TD2)
		 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
		 mat `se_3' = nullmat(`se_3'), r(se)
		 
		 
		 reghdfe `var'  TD1 TD2  `running' c.`running'#c.TD2  `if',  vce(cluster `clus_id') absorb(i.`strat_id')
		 test (_b[TD1]- _b[TD2]== 0)
		 mat `d_p2'  = nullmat(`d_p2'),r(p)
		 matrix A=e(b)
		 lincom (TD1-TD2)
		 mat `mu_4' = nullmat(`mu_4'), A[1,2]-A[1,1]
		 mat `se_4' = nullmat(`se_4'), r(se)
	 
	}
	foreach mat in mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2 {
		mat coln ``mat'' = `varlist'
	}
	 local cmd "my_ptest_sd"
	foreach mat in mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2 {
		eret mat `mat' = ``mat''
	}
end


use "$mipath/CreatedData/Temporary/controlDF_r3.dta", clear
*drop the Naya tc because population info is zero
drop if id_tc==153 | id_tc==162
/*All this cleanning was done in  R*/
drop if inReg!=1
keep if abs(distance)<2000

merge m:1 id_tc using "$mipath/CreatedData/bw_idtc.dta"
drop _merge
file open newfile using "$latexslides/obw.tex", read
file read newfile line
global optbw=`line'
file close newfile

replace bwtc=$optbw if bwtc==.


egen bin_distance=cut(distance), at(-2000(200)2000)
drop if missing(bin_distance)
replace disRoads=disRoads/1000
label var disRoads "Distance to nearest road (km)"
label var disRivers "Distance to nearest river (m)"
replace slope=slope/100
label var slope "Slope (\$ \% \$)"


gen Adentro=(distance<0)
/*
reghdfe elevation  Adentro  distance c.distance#c.Adentro  if distance<2000,  vce(cluster id_tc) absorb(i.id_tc) 
reghdfe elevation  Adentro  distance c.distance#c.Adentro  if distance<2000 & abs(dis_linea)<2000,  vce(cluster id_tc) absorb(i.id_tc) 
*/

/*
tab Adentro , gen(TD)
reg p00agpop  TD1 TD2 if distance<1000, nocons vce(cluster id_tc)
reghdfe p00agpop  TD1 TD2  if distance<1000,  vce(cluster id_tc) absorb(id_tc)
reghdfe elevation  TD1 TD2  distance c.distance#c.TD2  if distance<200,  vce(cluster id_tc) absorb(i.id_tc)
*/


foreach dist in 0.5 1 2 {
	eststo clear
	eststo: my_ptest_sd disRoads disRivers slope   if abs(distance)<=`dist'*bwtc, by(Adentro) running(distance) clus_id(id_tc) strat_id(id_tc)
	esttab using "$latexslides/tables/Balance_`dist'.tex" , label replace  nolines nogaps fragment ///
			nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
			cells("mu_2(fmt(%9.2fc)) mu_1(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p)) mu_4(fmt(%9.2fc) star pvalue(d_p2))" "se_2(par) se_1(par)  se_3(par) se_4(par)")
			esttab using "$tables/Balance_`dist'.tex" , label replace  nolines nogaps fragment ///
			nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
			cells("mu_2(fmt(%9.2fc)) mu_1(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p)) mu_4(fmt(%9.2fc) star pvalue(d_p2))" "se_2(par) se_1(par)  se_3(par) se_4(par)")

			/*
	eststo clear
	eststo: my_ptest_sd disRoads disRivers elevation     if abs(distance)<=`dist'*bwtc & abs(dis_linea)<`dist'*bwtc, by(Adentro) running(distance) clus_id(id_tc) strat_id(id_tc)
	esttab using "$latexslides/tables/Balance_`dist'_Linea.tex" , label replace  nolines nogaps fragment ///
			nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
			cells("mu_2(fmt(%9.2fc)) mu_1(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p)) mu_4(fmt(%9.2fc) star pvalue(d_p2))" "se_2(par) se_1(par)  se_3(par) se_4(par)")
esttab using "$tables/Balance_`dist'_Linea.tex" , label replace  nolines nogaps fragment ///
			nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
			cells("mu_2(fmt(%9.2fc)) mu_1(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p)) mu_4(fmt(%9.2fc) star pvalue(d_p2))" "se_2(par) se_1(par)  se_3(par) se_4(par)")
*/
			
			}

preserve
collapse (mean) disRoads disRivers elevation , by(bin_distance)


twoway ///
scatter elevation bin_distance  if bin_distance<0, graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter elevation bin_distance  if  bin_distance>=0 , mc(blue) || ///
lfit elevation bin_distance if bin_distance<0 , lc(black) || ///
lfit elevation bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2)) ytitle("Average elevation (mts)")
graph export "$latexslides/figures/VisualRD_Elevation.pdf", as(pdf) replace


twoway ///
scatter disRoads bin_distance  if bin_distance<0, graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter disRoads bin_distance  if  bin_distance>=0 , mc(blue) || ///
lfit disRoads bin_distance if bin_distance<0 , lc(black) || ///
lfit disRoads bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  ytitle("Average distance to nearest road (KM)")
graph export "$latexslides/figures/VisualRD_disRoads.pdf", as(pdf) replace

twoway ///
scatter disRivers bin_distance  if bin_distance<0, graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter disRivers bin_distance  if  bin_distance>=0 , mc(blue) || ///
lfit disRivers bin_distance if bin_distance<0 , lc(black) || ///
lfit disRivers bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  ytitle("Average distance to nearest river (mts)")
graph export "$latexslides/figures/VisualRD_disRivers.pdf", as(pdf) replace

restore


drop if abs(dis_linea)>2000
collapse (mean) disRoads disRivers elevation p00agpop p90agpop p95agpop, by(bin_distance)
twoway ///
scatter elevation bin_distance  if bin_distance<0, graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter elevation bin_distance  if  bin_distance>=0 , mc(blue) || ///
lfit elevation bin_distance if bin_distance<0 , lc(black) || ///
lfit elevation bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2)) ytitle("Average elevation (mts)")
graph export "$latexslides/figures/VisualRD_Elevation_linea.pdf", as(pdf) replace


twoway ///
scatter disRoads bin_distance  if bin_distance<0, graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter disRoads bin_distance  if  bin_distance>=0 , mc(blue) || ///
lfit disRoads bin_distance if bin_distance<0 , lc(black) || ///
lfit disRoads bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  ytitle("Average distance to nearest road (KM)")
graph export "$latexslides/figures/VisualRD_disRoads_linea.pdf", as(pdf) replace

twoway ///
scatter disRivers bin_distance  if bin_distance<0, graphregion(color(white))  mc(black) ms(square) msize(small) || ///
scatter disRivers bin_distance  if  bin_distance>=0 , mc(blue) || ///
lfit disRivers bin_distance if bin_distance<0 , lc(black) || ///
lfit disRivers bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2))  ytitle("Average distance to nearest river (mts)")
graph export "$latexslides/figures/VisualRD_disRivers_linea.pdf", as(pdf) replace



