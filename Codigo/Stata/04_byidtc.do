clear all
use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear

qui eststo clear

replace disRoads=disRoads/1000
label var disRoads "Distance to nearest road (km)"
label var disRivers "Distance to nearest river (mts)"
label var elevation "Elevation (mts)"
label var p00agpop "Population per pixel - 2000"
label var Htree2000 "\% with forest in 2000"



levelsof id_tc, local(levels) 
foreach l of local levels {
	capture reghdfe def inxpost d_in distance_in distance_in_sq distance_out distance_out_sq disRoads disRivers p00agpop elevation if abs(distance)<bwtc & id_tc==`l' , absorb(ano_idtc)
	if(_rc==0){
		mat A=e(b)
		mat varianza=e(V)
		mat Est = nullmat(Est)\ A[1,1]
		mat err = nullmat(err)\ varianza[1,1]
		mat ids = nullmat(ids)\ `l'
	}
}


svmat Est, name("Est")
svmat err, name("err")
svmat ids, name("ids")
keep Est err ids
drop if err==0 | err==.
sort Est
compress
save "$mipath/CreatedData/Est_id_tc.dta"

use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear

collapse (mean) disRoads disRivers p00agpop elevation habitantes familias area_sqkm ano_creatc (count) obs=def if abs(distance)<bwtc , by(id_tc)
rename id_tc ids1
merge 1:1 ids using "$mipath/CreatedData/Est_id_tc.dta"
drop if _merge!=3
drop _merge
replace err1=sqrt(err1)

gen hic= Est1+ err1*1.95
gen lic= Est1- err1*1.95
drop if area_sqkm==0

twoway (scatter Est1 area_sqkm [aweight = obs], mfcolor(none)) (lfit Est1 area_sqkm [aweight = obs]) (rcap lic hic area_sqkm, sort) if area_sqkm<103742.4
graph export  "$latexpaper/figures/reg_area.pdf", replace

twoway (scatter Est1 familias [aweight = obs], mfcolor(none)) (lfit Est1 familias [aweight = obs]) (rcap lic hic familias, sort) if area_sqkm<103742.4
graph export  "$latexpaper/figures/reg_familia.pdf", replace

sum area_sqkm,d
gen D=(area_sqkm>r(p50))

reg Est1 D [aw= obs]
