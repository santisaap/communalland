use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
hashsort  id_tc ano
fmerge m:1 id_tc using "$mipath/CreatedData/Temporary/tierras_comunales_fixed.dta", keepus( tercile* quintile* decile_area decile_habitantes  decile_densidad ) sorted keep(3) nogenerate 


qui eststo clear
qui eststo m_population:  reghdfe def inxpost_p*  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
qui tab id_tc if e(sample)
estadd local Ncom=r(r)
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"


qui eststo m_area:  reghdfe def inxpost_h*  distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
qui tab id_tc if e(sample)
estadd local Ncom=r(r)
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"
/*
tab d_h1 d_p1 if post_tc==0 & d_in==0 & abs(distance)<bwtc
tab d_h1 d_p1 if post_tc==0 & d_in==0 & abs(distance)<bwtc
tab d_h1 d_p1 if post_tc==0 & d_in==1 & abs(distance)<bwtc
tab d_h1 d_p1 if post_tc==1 & d_in==1 & abs(distance)<bwtc

reghdfe def c.d_in#c.post_tc##i.(d_h1 d_p1) distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) 


label var d_p1 "Small Pop"
qui eststo m_area_pop:  reghdfe def inxpost_h* inxpost_p* distance_in distance_out distance_in_sq  distance_out_sq disRoads disRivers slope if abs(distance)<bwtc , absorb(ano_idtc d_in##id_tc) vce(cluster ano_idtc )
qui estadd ysumm
qui tab id_tc if e(sample)
estadd local Ncom=r(r)
estadd local unit "Pixel"
estadd local timefe "\$h^*\$"
estadd local lawline "No"
estadd local poly "Quadratic"
*/

qui estout m_population  m_area     using "$tables/PopArea_Combined.tex" , style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none)  ///
keep(*inxpost_p*   *inxpost_h*  ) prefoot(\midrule ) stats( N Ncom ymean r2 , fmt(  %11.2gc a2 a2 a2 ) labels ("N. of obs." "Communities" "Mean of Dep. Var." "\$R^2\$" )) replace
