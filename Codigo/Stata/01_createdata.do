
*Year TC was requested

import excel "$mipath/CreatedData/tierras_comunales_fixed/tierras_comunales_fixed_SOLICITUD.xlsx", sheet("Sheet1") cellrange(A1:N165) firstrow clear

keep id_tc FECHA_SOLICITUD
drop if id_tc==.


gen ano_soltc=substr( FECHA_SOLICITUD,7,10)
destring ano_soltc, force replace
keep id_tc ano_soltc
label var ano_soltc "Year requested"

save "$mipath/CreatedData/Temporary/tierras_comunales_fixed_SOLICITUD.dta", replace

** Year the TC was created

import delimited "$mipath/CreatedData/tierras_comunales_fixed.csv", clear

merge 1:1 id_tc using "$mipath/CreatedData/Temporary/tierras_comunales_fixed_SOLICITUD.dta"
drop if _merge!=3
drop _merge

**Drop this TC because they arent in the main regression
*I guess they are surrounded by sea and other TCs so no control
drop if id_tc==25 | id_tc==26 | id_tc==30 | id_tc==55 | id_tc==65 | id_tc==66 | id_tc==152

rename ano ano_creatc
replace  area_of=area_of/100
replace  areacalc_sqkm=areacalc_sqkm/1000^2
rename area_of area_sqkm
label var ano_creatc "Year titled"
gen anos_soltocrea= ano_creatc-ano_soltc
label var anos_soltocrea "Years request to titling"
label var area_sqkm "Area (\$km^2\$)"


qui gen invarea=1/area_sqkm
label var invarea "Inverse area (sqkm^-1)"
qui sum area_sqkm , d
qui gen d_h1=(area_sqkm<=r(p50))

replace habitantes=. if habitantes==0
qui sum habitantes , d
qui gen d_p1=(habitantes<=r(p50)) if habitantes!=.

qui gen densidad=habitantes/area_sqkm
label var densidad "Density (Population per \$km^2\$)"
label var habitantes "Population"
qui sum densidad , d
qui gen d_d1=(densidad<=r(p50))


gquantiles decile_area = area_sqkm , xtile nquantiles(10) 
gquantiles decile_habitantes = habitantes , xtile nquantiles(10) 
gquantiles decile_densidad = densidad , xtile nquantiles(10) 

gquantiles quintile_area = area_sqkm , xtile nquantiles(5) 
gquantiles quintile_habitantes = habitantes , xtile nquantiles(5) 
gquantiles quintile_densidad = densidad , xtile nquantiles(5) 
 
 gquantiles tercile_area = area_sqkm , xtile nquantiles(3) 
gquantiles tercile_habitantes = habitantes , xtile nquantiles(3) 
gquantiles tercile_densidad = densidad , xtile nquantiles(3)
 
hashsort  id_tc
compress


save "$mipath/CreatedData/Temporary/tierras_comunales_fixed.dta", replace



keep id_tc ano_creatc ano_soltc area_sqkm invarea d_* areacalc_sqkm habitantes area_sqkm densidad tercile_habitantes

save "$mipath/CreatedData/Temporary/tierras_comunales_fixed_light.dta", replace
*/



use "$mipath/CreatedData/Temporary/pixelDF_r3.dta", clear
drop x y bufresfor bufindig bufnpark npark indig resfor ano_nparks ano_indig ano_resfor
*tree2000 c902000
drop if inReg!=1

*Slope comes from 0 to 10000 so we convert it to %
replace slope=slope/100

gen def2000=(Htree2000<50) & Htree2000!=.
forval i=2001/2016 {
local j=`i'-2000
*Deforested if it was tree before
gen def`i'=(lossyear<=`j')*(1-def2000)+def2000 

}

compress

gen id_pixel=_n
reshape long def, i(id_pixel) j(ano) 

merge m:1 id_tc using "$mipath/CreatedData/Temporary/tierras_comunales_fixed_light.dta"
drop if _merge!=3
drop _merge

gen d_in=( distance<=0)
label var d_in "Inner"
replace def=def*100
gen post_tc=( ano>= ano_creatc) if ano_creatc!=.
gen inxpost=d_in*post_tc
label var inxpost "After X Inner"
gen ano_idtc=ano*1000+id_tc
gen distance_in=-distance*d_in
gen distance_out=distance*(1-d_in)


qui gen inxpost_h1=inxpost*d_h1
label var inxpost_h1 "After X Inner X Small Area"
qui gen inxpost_h2=inxpost*(1-d_h1)
label var inxpost_h2 "After X Inner X Large Area"
qui gen d_in_h1=d_in*d_h1
label var d_in_h1 "Inner X Small Area"
qui gen d_in_h2=d_in*(1-d_h1)
label var d_in_h2 "Inner X Large Area"

qui gen inxpost_p1=inxpost*d_p1
label var inxpost_p1 "After X Inner X Small Pop"
qui gen inxpost_p2=inxpost*(1-d_p1)
label var inxpost_p2 "After X Inner X Large Pop"
qui gen d_in_p1=d_in*d_p1
label var d_in_p1 "Inner X Small Pop"
qui gen d_in_p2=d_in*(1-d_p1)
label var d_in_p2 "Inner X Large Pop"

qui gen inxpost_d1=inxpost*d_d1
label var inxpost_d1 "After X Inner X Small Dens"
qui gen inxpost_d2=inxpost*(1-d_d1)
label var inxpost_d2 "After X Inner X Large Dens"
qui gen d_in_d1=d_in*d_p1
label var d_in_d1 "Inner X Small Dens"
qui gen d_in_d2=d_in*(1-d_d1)
label var d_in_d2 "Inner X Large Dens"

compress

/*
***Optimal bandwith 
preserve
set more off
collapse def ,by(id_pixel distance post_tc id_tc d_h1)

** def is the dependent variable
** distance the running variable with the RD at 0
** post_tc=1 because we need def after the tc declaration
** There is no formula for RD with diff-diff


rdbwselect def distance if post_tc==1 
global optbw=round(e(h_mserd))
file open newfile using "$latexslides/obw.tex", write replace
file write newfile "$optbw"
file close newfile
rdbwselect def distance if post_tc==1  & d_h1==1
global optbwh1=round(e(h_mserd))
file open newfile using "$latexslides/obwh1.tex", write replace
file write newfile "$optbwh1"
file close newfile
rdbwselect def distance if post_tc==1  & d_h1==0
global optbwh2=round(e(h_mserd))
file open newfile using "$latexslides/obwh2.tex", write replace
file write newfile "$optbwh2"
file close newfile

***Optimal bandwith for each tc

gen bwtc=.

forval i=1/163 {
*if `i'~=1         {
if `i'~=1 & `i'~=3 & `i'~=9 & `i'~=25 & `i'~=26  & `i'~=30 & `i'~=40 & `i'~=51 & `i'~=55  & `i'~=58 & `i'~=65  & `i'~=66 & `i'~=116 & `i'~=152 {

display `i'
rdbwselect def distance if post_tc==1 & id_tc==`i'
replace bwtc=e(h_mserd) in `i'

}
}

keep bwtc
gen id_tc=_n
keep if id_tc<164
save "$mipath/CreatedData/bw_idtc.dta", replace

restore
*/

merge m:1 id_tc using "$mipath/CreatedData/bw_idtc.dta"
drop _merge 
compress

gen ratio=abs(distance)/bwtc

*Keep only the observations we use in the regression
*keep if abs(distance)<2*bwtc


label var bwtc "Opt. bandwith (m)"

drop if tree1990==. &  tree2000==. & tree2005==. & tree2010==.

foreach i in 1990 2000 2005 2010 {
replace tree`i'=. if tree`i'==3
replace tree`i' = 100*(tree`i'-1)
rename tree`i' defIDEAM`i'
}

file open newfile using "$latexslides/obw.tex", read
file read newfile line
global optbw=`line'
file close newfile

replace bwtc=$optbw if bwtc==.

gen distance_in_sq=distance_in*distance_in
gen distance_out_sq=distance_out*distance_out

replace disRoads=disRoads/1000
label var disRoads "Distance to nearest road (km)"
label var disRivers "Distance to nearest river (mts)"
label var elevation "Elevation (mts)"

label var Htree2000 "\% with forest in 2000"
save "$mipath/CreatedData/tc_pixel5k_r3.dta", replace




