
use "$mipath/CreatedData/Temporary/controlDF_r3.dta", clear
drop if abs(distance)!=abs(dis_out)
keep if abs(distance)<2000
egen bin_distance=cut(distance), at(-2000(200)2000)
drop if missing(bin_distance)

levelsof id_tc, local(levels)
foreach l of local levels {
	preserve
	drop if id_tc!=`l'


	collapse (mean)  elevation , by(bin_distance)
	gen Adentro=(bin_distance<0)
	
	reg elevation c.bin_distance##c.Adentro
	test c.bin_distance#c.Adentro
	if(r(p)<0.05){
		twoway ///
		scatter elevation bin_distance  if bin_distance<0, graphregion(color(white))  mc(black) ms(square) msize(small) || ///
		scatter elevation bin_distance  if  bin_distance>=0 , mc(blue) || ///
		lfit elevation bin_distance if bin_distance<0 , lc(black) || ///
		lfit elevation bin_distance if bin_distance>=0 , lc(blue) xtitle("Distance to border (meters)") xline(0) legend(order(1 2 ) rows(2)) ytitle("Average elevation (mts)")
		graph export "$latexslides/figures/VisualRD_Elevation`l'.pdf", as(pdf) replace
	}
	restore
}
