capt prog drop my_ptest_sd
	*! version 1.0.0  14aug2007  Ben Jann
	program my_ptest_sd, eclass
	*clus(clus_var)
	syntax varlist [if] [in], by(varname) running(varname numeric) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

	marksample touse
	markout `touse' `by'
	tempname mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2
	capture drop TD*
	tab `by' , gen(TD)
	foreach var of local varlist {
		 reg `var'  TD1 TD2   `if', nocons vce(cluster `clus_id')
		 matrix A=e(b)
		 lincom (TD1-TD2)
		 sum `var' if TD2==1 & e(sample)==1
		 mat `mu_1' = nullmat(`mu_1'), r(mean)
		 mat `se_1' = nullmat(`se_1'), r(sd)
		 sum `var' if TD1==1 & e(sample)==1
		 mat `mu_2' = nullmat(`mu_2'),r(mean)
		 mat `se_2' = nullmat(`se_2'), r(sd)
		 
		 reg `var'  TD1 TD2   `if',  vce(cluster `clus_id')
		 test (_b[TD1]- _b[TD2]== 0)
		 mat `d_p'  = nullmat(`d_p'),r(p)
		 matrix A=e(b)
		 lincom (TD1-TD2)
		 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
		 mat `se_3' = nullmat(`se_3'), r(se)
		 
		 
		 reghdfe `var'  TD1 TD2  `running' c.`running'#c.TD2  `if',  vce(cluster `clus_id') absorb(i.`strat_id')
		 test (_b[TD1]- _b[TD2]== 0)
		 mat `d_p2'  = nullmat(`d_p2'),r(p)
		 matrix A=e(b)
		 lincom (TD1-TD2)
		 mat `mu_4' = nullmat(`mu_4'), A[1,2]-A[1,1]
		 mat `se_4' = nullmat(`se_4'), r(se)
	 
	}
	foreach mat in mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2 {
		mat coln ``mat'' = `varlist'
	}
	 local cmd "my_ptest_sd"
	foreach mat in mu_1 mu_2 mu_3 mu_4 se_1 se_2 se_3 se_4 d_p d_p2 {
		eret mat `mat' = ``mat''
	}
end

use "$mipath/CreatedData/tc_pixel5k_r3.dta", clear
drop if inReg!=1
keep if abs(distance)<2000

gen defBefore=def if post_tc==0
gen defAfter=def if post_tc==1

keep def defAfter defBefore id_pixel id_tc distance
collapse (mean) def defAfter defBefore, by(id_pixel id_tc distance)

merge m:1 id_tc using "$mipath/CreatedData/bw_idtc.dta"
drop _merge
file open newfile using "$latexslides/obw.tex", read
file read newfile line
global optbw=`line'
file close newfile

replace bwtc=$optbw if bwtc==.

gen Adentro=(distance<0)


label var defBefore "Not covered by forest before titling (\%)"
label var defAfter "Not covered by forest after titling (\%)"

sum defBefore if Adentro==0  & abs(distance)<=bwtc
local temp=string(r(mean), "%9.2gc")
file open newfile using "$tables/DefoOutBefore.tex", write replace
file write newfile "`temp'"
file close newfile	

sum defBefore if Adentro==1  & abs(distance)<=bwtc
local temp=string(r(mean), "%9.2gc")
file open newfile using "$tables/DefoInBefore.tex", write replace
file write newfile "`temp'"
file close newfile	


sum defAfter if Adentro==0  & abs(distance)<=bwtc
local temp=string(r(mean), "%9.2gc")
file open newfile using "$tables/DefoOutAfter.tex", write replace
file write newfile "`temp'"
file close newfile	

sum defAfter if Adentro==1 & abs(distance)<=bwtc
local temp=string(r(mean), "%9.2gc")
file open newfile using "$tables/DefoInAfter.tex", write replace
file write newfile "`temp'"
file close newfile	


foreach dist in 0.5 1 2 {
	eststo clear
	eststo: my_ptest_sd defBefore defAfter     if abs(distance)<=`dist'*bwtc, by(Adentro) running(distance) clus_id(id_tc) strat_id(id_tc)
	esttab  using "$latexslides/tables/BalanceDeforest_`dist'.tex", label replace  nolines nogaps fragment ///
			nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
			cells("mu_2(fmt(%9.2fc)) mu_1(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p)) mu_4(fmt(%9.2fc) star pvalue(d_p2))" "se_2(par) se_1(par)  se_3(par) se_4(par)")
			esttab using "$tables/BalanceDeforest_`dist'.tex" , label replace  nolines nogaps fragment ///
			nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
			cells("mu_2(fmt(%9.2fc)) mu_1(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p)) mu_4(fmt(%9.2fc) star pvalue(d_p2))" "se_2(par) se_1(par)  se_3(par) se_4(par)")
}
