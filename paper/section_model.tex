Assume there are $n$ agents, each choosing how much to produce in an activity that affects the forest around them. The total endowment of forest in the community is $F$. Each agent derives utility $g(\cdot)$ from consuming forest goods ($f_i$), as well as utility $h(\cdot)$ from the amount of remaining standing forest ($F-\sum_{i=1}^n f_i$) via natural services. Extracting forest goods has a cost ($c(f_i)$). The total utility of agent $i$ is thus equal to:

\begin{equation}
u(f_i,f_{-i})=g(f_i)+h(F-\sum_{i=1}^n f_i)-c(f_i)
\end{equation}

In the case of a pure communal resource, this framework is typically used to explain the tragedy of the commons. In a symmetric Nash equilibrium:

\begin{equation}
g'(f^*)=h'(F-nf^*)+c'(f^*)
\end{equation}

While the social optimum is:

\begin{equation}
g'(f^*)=n h'(F-n f^*)+c'(f^*)
\end{equation}

Thus, there is more deforestation in the Nash equilibrium than in the social optimum. Below, we examine how the conclusions of the model change when we introduce communal titling. 


\subsection{Common resource titling}

Now assume that the forest is titled to $n_o$ of the agents. For ease of exposition, we assume that only the owners get utility from standing forest, and that the other agents get utility only from the forest they consume privately. If we allow the standing forest to produce benefits for non-owners, the qualitative conclusions of the model are unchanged, but the overall level of deforestation is lower. The key is that owners obtain higher benefits from standing forest than non-forest owners. For example, non-owners receive water and air filtering, while owners can also enjoy the benefits of fruit gathering and eco-tourism. 
\\[0.4cm]
The forest owners can exert some effort ($e_i$) to keep the $n-n_o$ non-owner agents away from the forest. The total monitoring of the title is $E=\sum_{j=1}^{n_o} e_j$. The monitoring by title owners implies that an outsider only obtains $f_i/(1+E)$ when spending $f_i$ time deforesting.

The utility of forest owners is:

\begin{equation}
u_1(f_i,f_{-i},e_i,e_{-i})=g (f_i)+h\left( F-\sum_{j=1}^{n_o} f_j-\frac{1}{1+\sum_{j=1}^{n_o} e_j} \sum_{j=n_o+1}^{n} f_j \right)-c(f_i,e_i)
\end{equation}
and the utility for one of the other $n-n_o$ agents is:
\begin{equation}
u_2(f_i,e)=g \left( \frac{f_i}{1+\sum_{j=1}^{n_o} e_j} \right)-c(f_i,0)
\end{equation}

The first-order conditions for the forest owners are:

\begin{eqnarray}
g'(f_i)-h'\left( F-\sum_{j=1}^{n_o} f_j-\frac{1}{1+\sum_{j=1}^{n_o} e_j} \sum_{j=n_o+1}^{n} f_j \right)&=&c'_f(f_i,e_i)\\
\frac{\sum_{j=n_o+1}^{n} f_j }{\left(1+ \sum_{j=1}^{n_o} e_j \right)^2} h'\left( F-\sum_{j=1}^{n_o} f_j-\frac{1}{1+\sum_{j=1}^{n_o} e_j} \sum_{j=n_o+1}^{n} f_j \right)&=&c'_e(f_i,e_i)
\end{eqnarray}

and for the non-owners (the other $n-n_o$ agents):

\begin{equation}
\left(\frac{1}{1+\sum_{i=1}^{n_o} e_i} \right) g'\left( \frac{f_i}{1+\sum_{i=1}^{n_o} e_i} \right)=c'_f(f_i,0)
\end{equation}

In a symmetric equilibrium, where $f^*$ is the optimal quantity of forest consumed by the forest owners and $f^{**}$ is the optimal quantity of forest consumed by the other $n-n_o$ agents, we have:

\begin{eqnarray}
\underbrace{c'_f(f^{*},e^*)+h'\left( F-n_of^*-\frac{(n-n_o)f^{**}}{1+n_oe^*} \right)}_{\text{marginal cost private consumption}} &=& \underbrace{g'(f^*)}_{\text{marginal benefit private consumption}} \label{eq:focn1_1} \\
\underbrace{\frac{(n-n_o) f^{**} }{(1+n_o e^*)^2 } h'\left( F-n_o f^{*}- \frac{(n-n_o)f^{**}}{ 1+n_oe^*} \right)}_{\text{marginal benefit keeping outsiders out}}&=&\underbrace{c'_e(f^{*},e^*)}_{\text{marginal cost keeping outsiders out}} \label{eq:focn1_2} \\
\underbrace{\left(\frac{1}{1+n_o e^*} \right) g'\left( \frac{f^{**}}{1+n_o e^*} \right)}_{\text{outsiders' marginalbenefit private consumption}} &=& \underbrace{c'_f(f^{**},0)}_{\text{outsiders' marginalcost private consumption}}
\end{eqnarray}

In short, both owners and non-owners deforest until the marginal benefit of doing so is equal to the marginal cost. In addition, forest owners exert effort keeping outsiders out until the marginal benefit of doing so is equal to the marginal cost. 

\subsection{Comparative statics}

We solve numerically, assuming $g(f)=f^{\alpha_g}$, $h(f)=b_hf^{\alpha_h}$ and $c(f,e)=b_c(f+b_ee)^{\alpha_c}$, and show how the resulting equilibria vary as we vary the valuation of standing forest ($b_h$). In Figures \ref{fig:th_defT}-\ref{fig:th_defbygroup} we assume $\alpha_g=0.5$, $\alpha_h=0.5$ (i.e., concave returns from forest product consumption and standing forest) and $\alpha_c=2$ (i.e., convex costs of effort). 
\\[0.4cm]
Figure \ref{fig:th_defT} shows total deforestation for different values of the forest service's valuation parameter: low ($b_h=1$), medium ($b_h=2$), and high ($b_h=3$). The solid red line of low valuation is always above the blue dashed line, which is always above the dotted green line. That is, for any number of titled agents, deforestation is higher for low levels of forest valuation. When the forest service's valuation is low, deforestation increases with the number of agents titled. Intuitively, the private value of cutting trees is higher than keeping them standing. However, when the forest service's valuation is high, deforestation decreases when there are more owners. Intuitively, the forest is very valuable: The gains from ecosystem services outweigh the private benefits of cutting down trees. For the intermediate case, there is a decrease in deforestation for small $N$, but for larger values deforestation increases. Intuitively, at first more owners share the cost of preventing outsiders from logging the forest, which allows them to protect more of the forest. Eventually, however, as the number of owners grows the tragedy of the commons effect becomes more prevalent and deforestation increases.


\begin{figure}[H]
\begin{center}
\caption{Deforestation and the number of owners of communal title\label{fig:th_defT}}
\includegraphics[width=0.6\textwidth]{figures/sm/Totaldef.png}
\begin{figurenotes}
\textit{Notes:} The x-axis plots the number of owners and the y-axis total deforestation. The curves differ on the level of forest service's valuation ($b_h$). The higher curve indicates greater deforestation due to lower forest valuation.
\end{figurenotes}
\end{center}
\end{figure}

To understand the mechanisms behind these results, we separately show the monitoring effort and deforestation in Figures \ref{fig:th_effort} and \ref{fig:th_defbygroup}, respectively. Figure \ref{fig:th_Indive} presents individual monitoring effort as a function of the number of owners of the title, for different levels of forest valuation. As expected, individuals monitor more when the valuation of the standing forest is high. But each individual spends less time monitoring when there are more members since he benefits from his co-owners' monitoring efforts. This individual behavior causes total monitoring effort to display an inverted U-shape pattern: it increases with $N$ for small communities, but eventually declines as $N$ becomes larger (Figure \ref{fig:th_Totale}).

\begin{figure}[H]
\centering
\caption{Monitoring effort and number of owners of communal title\label{fig:th_effort}}
\begin{subfigure}{0.48\textwidth}
\centering
\includegraphics[width=\textwidth]{figures/sm/Indive.png}
\caption{Individual monitoring effort \label{fig:th_Indive}}
\end{subfigure}
\hfill
\begin{subfigure}{0.48\textwidth}
\centering
\includegraphics[width=\textwidth]{figures/sm/Totale.png}
\caption{Total monitoring effort\label{fig:th_Totale}}
\end{subfigure}
\begin{minipage}{1\textwidth}
\scriptsize{\textit{Notes:} The x-axis indicates the number of owners of the communal title and the y-axis represents effort. The left panel plots individual monitoring and the right panel total monitoring. The curves differ on the level of forest service's valuation ($b_h$). Individual and total effort are greater for higher levels of forest valuation.}
\end{minipage}
\end{figure}

Figure \ref{fig:th_defind_bygroup} plots individual deforestation by owners and outsiders when forest valuation is high. Deforestation by an outsider is inversely related to total monitoring effort and displays a U shape (Figure \ref{fig:th_Totale}). In addition, outsiders always deforest more than insiders because they do not enjoy the benefits of the standing forest. Finally, the owners' individual deforestation increases when there are more owners, as in the classic tragedy of the commons. For each group, total deforestation depends mainly on the number of members of each group (see Figure \ref{fig:th_defT_bygroup}).

\begin{figure}[H]
\centering
\caption{Outsider and owner deforestation by number of titled individuals\label{fig:th_defbygroup}}
\begin{subfigure}{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{figures/sm/defind_bygroup.png}
\caption{Individual deforestation by group\label{fig:th_defind_bygroup}}
\end{subfigure}
\hfill
\begin{subfigure}{0.49\textwidth}
\centering
\includegraphics[width=\textwidth]{figures/sm/defT_bygroup.png}
\caption{Total deforestation by group \label{fig:th_defT_bygroup}}
\end{subfigure}
\begin{minipage}{1\textwidth}
\scriptsize{\textit{Notes:} The x-axis indicates the number of owners of the communal title and the y-axis represents deforestation. The left panel plots individual deforestation and the right panel total deforestation by group. The solid blue line indicates deforestation by outsiders, and the dashed green line deforestation by title owners.}
\end{minipage}
\end{figure}

To summarize, there is a non-monotonic relationship between the number of owners of communal land title and deforestation. At first, deforestation decreases as the number of owners increases, but eventually deforestation increases as the number of owners continues to increase. If owners value standing forest more, the turning point (from decreasing to increasing deforestation) occurs with larger communities. 
