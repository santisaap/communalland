 

We present a simple framework of how communal land titling affects the use of forest resources. The main goal of the model is to understand the effect of communal titling on deforestation, and how this effect is mediated by the number of title owners and the size of the titled area. The model is similar to the one developed by \citeA{DasguptaHeal1979}, with identical agents and individual utility that depends on a person's own actions and those of others using the resource. However, our model has two extra features. First, it allows for the possibility that outsiders (i.e., those who do not own the title) are responsible for part of the deforestation. Second, it takes into account each owner's decision about whether to allocate time to either deforest or monitor deforestation from non-owners. In the supplementary material (Appendix \ref{asec:model}) we provide a formal mathematical setup.
\\[0.4cm]
Each agent derives utility from consuming forest goods, and from the amount of remaining standing forest. In the case of a pure communal resource, this framework yields the standard tragedy of the commons result: there is more deforestation in the Nash Equilibrium than in the social optimum. 
\\[0.4cm]
We expand the model to study the effects of introducing communal titling. Besides exploiting the forest for their own consumption, title owners can exert some effort monitoring to prevent non-owners from deforesting inside the title. Monitoring reduces the forest goods obtained by outsiders per unit of deforestation effort. As the number of title owners grows, each owner monitors for less time, as he benefits from his co-owners' monitoring efforts. This behavior causes total monitoring effort to display an inverted U-shape pattern: it increases with more owners for small communities, but eventually declines when the free-riding effect on monitoring dominates. Figure \ref{fig:th_effort} in Appendix \ref{asec:model} illustrates this pattern.  Since the deforestation caused by an outsider (i.e., non-title holder) is inversely related to the total monitoring effort it displays a U-shape pattern. Finally, owners' individual deforestation increases when there are more owners, as in the classic tragedy of the commons.
\\[0.4cm]
Although theoretically there is an U-shape relationship between the number of owners and deforestation, for the parameter space in our empirical application an increase in population is generally associated with more deforestation. Specifically, the smallest communal title has more than 65 individuals, which is beyond the population that minimizes deforestation in our simulations. 
\\[0.4cm]
Figure \ref{fig:heatmap} shows the reduction in deforestation (in percentage terms) associated with titling. Each grid point represents a reduction in deforestation after titling, depending on the number of title owners and the size of the titled area. The vertical axis increases the number of owners from bottom to top. The horizontal axis increases the area of forest that is titled from left to right. As the number of owners grows, free-riding on monitoring and the tragedy of the commons effect become more prevalent; total deforestation thus increases. Likewise, for a fixed number of owners, deforestation increases as the forest titled area increases.

\begin{figure}[H]
\begin{center}
\caption{The effect of titling on deforestation\label{fig:heatmap}}
\includegraphics[width=0.8\textwidth]{figures/HeatPlot.pdf}
\begin{figurenotes}
{\footnotesize
\textit{Notes:} Heatmap of the reduction in deforestation associated with titling. Each grid point represents the percentage reduction in deforestation after titling associated with a given number of title owners and forest area. The y-axis plots the number of owners and the y-axis the total titled forest area. The darker the green, the larger the percentage reduction in deforestation. White indicates no difference from the non-title case. Red represents an increase in deforestation.}
\end{figurenotes}
\end{center}
\end{figure}