\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Agrawal%
}{%
Agrawal%
}{%
{\protect \APACyear {2001}}%
}]{%
AGRAWAL20011649}
\APACinsertmetastar {%
AGRAWAL20011649}%
\begin{APACrefauthors}%
Agrawal, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Common Property Institutions and Sustainable Governance
  of Resources} {Common property institutions and sustainable governance of
  resources}.{\BBCQ}
\newblock
\APACjournalVolNumPages{World Development}{29}{10}{1649 - 1672}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Agrawal%
\ \BBA {} Goyal%
}{%
Agrawal%
\ \BBA {} Goyal%
}{%
{\protect \APACyear {2001}}%
}]{%
agrawal2001group}
\APACinsertmetastar {%
agrawal2001group}%
\begin{APACrefauthors}%
Agrawal, A.%
\BCBT {}\ \BBA {} Goyal, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2001}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Group size and collective action: Third-party monitoring
  in common-pool resources} {Group size and collective action: Third-party
  monitoring in common-pool resources}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Comparative Political Studies}{34}{1}{63--93}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Alix-Garcia%
}{%
Alix-Garcia%
}{%
{\protect \APACyear {2007}}%
}]{%
Alix2007}
\APACinsertmetastar {%
Alix2007}%
\begin{APACrefauthors}%
Alix-Garcia, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A spatial analysis of common property deforestation} {A
  spatial analysis of common property deforestation}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Environmental Economics and
  Management}{53}{2}{141--157}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Andam%
, Ferraro%
, Pfaff%
, Sanchez-Azofeifa%
\BCBL {}\ \BBA {} Robalino%
}{%
Andam%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2008}}%
}]{%
Andam_etal2008}
\APACinsertmetastar {%
Andam_etal2008}%
\begin{APACrefauthors}%
Andam, K\BPBI S.%
, Ferraro, P\BPBI J.%
, Pfaff, A.%
, Sanchez-Azofeifa, G\BPBI A.%
\BCBL {}\ \BBA {} Robalino, J\BPBI A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2008}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Measuring the effectiveness of protected area networks
  in reducing deforestation} {Measuring the effectiveness of protected area
  networks in reducing deforestation}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Proceedings of the national academy of
  sciences}{105}{42}{16089--16094}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Barcelo%
\ \BBA {} Capraro%
}{%
Barcelo%
\ \BBA {} Capraro%
}{%
{\protect \APACyear {2015}}%
}]{%
barcelo2015group}
\APACinsertmetastar {%
barcelo2015group}%
\begin{APACrefauthors}%
Barcelo, H.%
\BCBT {}\ \BBA {} Capraro, V.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Group size effect on cooperation in one-shot social
  dilemmas} {Group size effect on cooperation in one-shot social
  dilemmas}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Scientific reports}{5}{1}{1--8}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Barsimantov%
\ \BBA {} Kendall%
}{%
Barsimantov%
\ \BBA {} Kendall%
}{%
{\protect \APACyear {2012}}%
}]{%
Barsimantovetal2012}
\APACinsertmetastar {%
Barsimantovetal2012}%
\begin{APACrefauthors}%
Barsimantov, J.%
\BCBT {}\ \BBA {} Kendall, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Community forestry, common property, and deforestation
  in eight {Mexican} states} {Community forestry, common property, and
  deforestation in eight {Mexican} states}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The Journal of Environment \&
  Development}{21}{4}{414--437}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
BenYishay%
, Heuser%
, Runfola%
\BCBL {}\ \BBA {} Trichler%
}{%
BenYishay%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
BenYishay_etal2017}
\APACinsertmetastar {%
BenYishay_etal2017}%
\begin{APACrefauthors}%
BenYishay, A.%
, Heuser, S.%
, Runfola, D.%
\BCBL {}\ \BBA {} Trichler, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Indigenous land rights and deforestation: Evidence from
  the {Brazilian Amazon}} {Indigenous land rights and deforestation: Evidence
  from the {Brazilian Amazon}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Environmental Economics and
  Management}{86}{}{29--47}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Blackman%
, Corral%
, Lima%
\BCBL {}\ \BBA {} Asner%
}{%
Blackman%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
Blackman_etal2017}
\APACinsertmetastar {%
Blackman_etal2017}%
\begin{APACrefauthors}%
Blackman, A.%
, Corral, L.%
, Lima, E\BPBI S.%
\BCBL {}\ \BBA {} Asner, G\BPBI P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Titling indigenous communities protects forests in the
  {Peruvian Amazon}} {Titling indigenous communities protects forests in the
  {Peruvian Amazon}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Proceedings of the National Academy of
  Sciences}{114}{16}{4123--4128}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Blackman%
\ \BBA {} Veit%
}{%
Blackman%
\ \BBA {} Veit%
}{%
{\protect \APACyear {2018}}%
}]{%
BlackmanVeit2018}
\APACinsertmetastar {%
BlackmanVeit2018}%
\begin{APACrefauthors}%
Blackman, A.%
\BCBT {}\ \BBA {} Veit, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Amazon Indigenous Communities Cut Forest Carbon
  Emissions} {Amazon indigenous communities cut forest carbon
  emissions}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Ecological Economics}{153}{}{56--67}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bonilla-Mejia%
\ \BBA {} Higuera-Mendieta%
}{%
Bonilla-Mejia%
\ \BBA {} Higuera-Mendieta%
}{%
{\protect \APACyear {2019}}%
}]{%
BonillaHiguera2019}
\APACinsertmetastar {%
BonillaHiguera2019}%
\begin{APACrefauthors}%
Bonilla-Mejia, L.%
\BCBT {}\ \BBA {} Higuera-Mendieta, I.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Protected Areas under Weak Institutions: Evidence from
  {Colombia}} {Protected areas under weak institutions: Evidence from
  {Colombia}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{World Development}{122}{}{585 - 596}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Busch%
\ \BBA {} Ferretti-Gallon%
}{%
Busch%
\ \BBA {} Ferretti-Gallon%
}{%
{\protect \APACyear {2017}}%
}]{%
Buschetal2017}
\APACinsertmetastar {%
Buschetal2017}%
\begin{APACrefauthors}%
Busch, J.%
\BCBT {}\ \BBA {} Ferretti-Gallon, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {What drives deforestation and what stops it? A
  meta-analysis} {What drives deforestation and what stops it? a
  meta-analysis}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Review of Environmental Economics and
  Policy}{11}{1}{3--23}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Calonico%
, Cattaneo%
, Farrell%
\BCBL {}\ \BBA {} Titiunik%
}{%
Calonico%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
Calonicoetal2017}
\APACinsertmetastar {%
Calonicoetal2017}%
\begin{APACrefauthors}%
Calonico, S.%
, Cattaneo, M\BPBI D.%
, Farrell, M\BPBI H.%
\BCBL {}\ \BBA {} Titiunik, R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {rdrobust: Software for regression-discontinuity designs}
  {rdrobust: Software for regression-discontinuity designs}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Stata Journal}{}{17}{372-404}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Ch\'avez%
, Murphy%
\BCBL {}\ \BBA {} Stranlund%
}{%
Ch\'avez%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2019}}%
}]{%
Chavez_etal2019}
\APACinsertmetastar {%
Chavez_etal2019}%
\begin{APACrefauthors}%
Ch\'avez, C\BPBI A.%
, Murphy, J\BPBI J.%
\BCBL {}\ \BBA {} Stranlund, J\BPBI K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
\APACrefbtitle {{Co-enforcement of Common Pool Resources: Experimental Evidence
  from TURFs in Chile}} {{Co-enforcement of Common Pool Resources: Experimental
  Evidence from TURFs in Chile}}\ \APACbVolEdTR {}{Working Papers\ \BNUM\
  19-18}.
\newblock
\APACaddressInstitution{}{Chapman University, Economic Science Institute}.
\newblock
\begin{APACrefURL} \url{https://ideas.repec.org/p/chu/wpaper/19-18.html}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chomitz%
\ \BBA {} Gray%
}{%
Chomitz%
\ \BBA {} Gray%
}{%
{\protect \APACyear {1996}}%
}]{%
Chomitz1996}
\APACinsertmetastar {%
Chomitz1996}%
\begin{APACrefauthors}%
Chomitz, K\BPBI M.%
\BCBT {}\ \BBA {} Gray, D\BPBI A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1996}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Roads, Land Use, and Deforestation: A Spatial Model
  Applied to {B}elize} {Roads, land use, and deforestation: A spatial model
  applied to {B}elize}.{\BBCQ}
\newblock
\APACjournalVolNumPages{The World Bank Economic Review}{10}{3}{487--512}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Congreso de Colombia}%
}{%
{Congreso de Colombia}%
}{%
{\protect \APACyear {1993}}%
}]{%
Ley701993}
\APACinsertmetastar {%
Ley701993}%
\begin{APACrefauthors}%
{Congreso de Colombia}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1993}{}{}.
\newblock
\APACrefbtitle {Ley 70 de 1993.} {Ley 70 de 1993.}
\newblock
\APAChowpublished {Diario Oficial No. 41.013}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Dasgupta%
\ \BBA {} Heal%
}{%
Dasgupta%
\ \BBA {} Heal%
}{%
{\protect \APACyear {1979}}%
}]{%
DasguptaHeal1979}
\APACinsertmetastar {%
DasguptaHeal1979}%
\begin{APACrefauthors}%
Dasgupta, P.%
\BCBT {}\ \BBA {} Heal, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{1979}.
\newblock
\APACrefbtitle {Economic Theory and Exhaustible Resources} {Economic theory and
  exhaustible resources}.
\newblock
\APACaddressPublisher{}{Cambridge University Press}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
De~Janvry%
, Sadoulet%
\BCBL {}\ \BBA {} Wolford%
}{%
De~Janvry%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2001}}%
}]{%
DeJanvrySadoulet2001}
\APACinsertmetastar {%
DeJanvrySadoulet2001}%
\begin{APACrefauthors}%
De~Janvry, A.%
, Sadoulet, E.%
\BCBL {}\ \BBA {} Wolford, W.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2001}.
\newblock
\APACrefbtitle {Access to land and land policy reforms} {Access to land and
  land policy reforms}\ (\BVOL~3).
\newblock
\APACaddressPublisher{}{UNU World Institute for Development Economics Research
  Helsinki}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{DIVA-GIS}%
}{%
{DIVA-GIS}%
}{%
{\protect \APACyear {2019}}%
}]{%
divagis}
\APACinsertmetastar {%
divagis}%
\begin{APACrefauthors}%
{DIVA-GIS}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
\APACrefbtitle {Roads in {C}olombia.} {Roads in {C}olombia.}
\newblock
\begin{APACrefURL}
  [{06/01/2019}]\url{http://biogeo.ucdavis.edu/data/diva/rds/COL_rds.zip}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{El Tiempo}%
}{%
{El Tiempo}%
}{%
{\protect \APACyear {2016}}%
}]{%
El2016}
\APACinsertmetastar {%
El2016}%
\begin{APACrefauthors}%
{El Tiempo}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
\APACrefbtitle {As\'i viven en la {C}olombia a la que no se puede llegar por
  carretera.} {As\'i viven en la {C}olombia a la que no se puede llegar por
  carretera.}
\newblock
\begin{APACrefURL}
  [{19/12/2019}]\url{https://www.eltiempo.com/colombia/otras-ciudades/colombia-pueblos-sin-vias-de-comunicacion-46383}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Galindo%
, Espejo%
, Rubiano%
, Vergara%
\BCBL {}\ \BBA {} Cabrera%
}{%
Galindo%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
IDEAM2014}
\APACinsertmetastar {%
IDEAM2014}%
\begin{APACrefauthors}%
Galindo, G.%
, Espejo, O.%
, Rubiano, J.%
, Vergara, L.%
\BCBL {}\ \BBA {} Cabrera, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
\APACrefbtitle {Protocolo de procesamiento digital de im{\'a}genes para la
  cuantificaci{\'o}n de la deforestaci{\'o}n en {Colombia} {V}2.0} {Protocolo
  de procesamiento digital de im{\'a}genes para la cuantificaci{\'o}n de la
  deforestaci{\'o}n en {Colombia} {V}2.0}\ \APACbVolEdTR {}{techreport}.
\newblock
\APACaddressInstitution{}{Instituto de Hidrolog\'ia, Meteorolog\'ia y Estudios
  Ambientales --- IDEAM. Bogot\'a D.C., Colombia}.
\newblock
\begin{APACrefURL}
  [{01/06/2019}]\url{http://www.ideam.gov.co/documents/11769/44688974/Protocolo+de+PDI+para+la+cuantificacion+de+la+deforestacion+en+colombia+v2_1_.pdf/00b95004-53dd-49f9-ab09-16d8803ccd92?version=1.0}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Gilmour%
}{%
Gilmour%
}{%
{\protect \APACyear {2016}}%
}]{%
Gilmour2016}
\APACinsertmetastar {%
Gilmour2016}%
\begin{APACrefauthors}%
Gilmour, D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2016}.
\newblock
\APACrefbtitle {Forty years of community-based forestry: A review of its extent
  and effectiveness} {Forty years of community-based forestry: A review of its
  extent and effectiveness}.
\newblock
\APACaddressPublisher{}{Food and Agriculture Organization of the United
  Nations}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Goodman-Bacon%
}{%
Goodman-Bacon%
}{%
{\protect \APACyear {2018}}%
}]{%
NBERw25018}
\APACinsertmetastar {%
NBERw25018}%
\begin{APACrefauthors}%
Goodman-Bacon, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
\APACrefbtitle {Difference-in-Differences with Variation in Treatment Timing}
  {Difference-in-differences with variation in treatment timing}\ \APACbVolEdTR
  {}{Working Paper\ \BNUM\ 25018}.
\newblock
\APACaddressInstitution{}{National Bureau of Economic Research}.
\newblock
\begin{APACrefURL} \url{http://www.nber.org/papers/w25018} \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hansen%
\ \protect \BOthers {.}}{%
Hansen%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Hansen_etal}
\APACinsertmetastar {%
Hansen_etal}%
\begin{APACrefauthors}%
Hansen, M\BPBI C.%
, Potapov, P\BPBI V.%
, Moore, R.%
, Hancher, M.%
, Turubanova, S\BPBI A.%
, Tyukavina, A.%
\BDBL {}Townshend, J\BPBI R\BPBI G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {High-Resolution Global Maps of 21st-Century Forest Cover
  Change} {High-resolution global maps of 21st-century forest cover
  change}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science}{342}{6160}{850--853}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Instituto de Hidrolog\'ia, Meteorolog\'ia y Estudios Ambientales (IDEAM)}%
}{%
{Instituto de Hidrolog\'ia, Meteorolog\'ia y Estudios Ambientales (IDEAM)}%
}{%
{\protect \APACyear {2019}}%
}]{%
ideam}
\APACinsertmetastar {%
ideam}%
\begin{APACrefauthors}%
{Instituto de Hidrolog\'ia, Meteorolog\'ia y Estudios Ambientales (IDEAM)}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
\APACrefbtitle {Cobertura de Bosque No Bosque.} {Cobertura de bosque no
  bosque.}
\newblock
\begin{APACrefURL} [{01/06/2019}]\url{http://www.ideam.gov.co/capas-geo}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Jones%
\ \BBA {} Lewis%
}{%
Jones%
\ \BBA {} Lewis%
}{%
{\protect \APACyear {2015}}%
}]{%
JonesLewis(2015)}
\APACinsertmetastar {%
JonesLewis(2015)}%
\begin{APACrefauthors}%
Jones, K\BPBI W.%
\BCBT {}\ \BBA {} Lewis, D\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Estimating the Counterfactual Impact of Conservation
  Programs on Land Cover Outcomes: The Role of Matching and Panel Regression
  Techniques} {Estimating the counterfactual impact of conservation programs on
  land cover outcomes: The role of matching and panel regression
  techniques}.{\BBCQ}
\newblock
\APACjournalVolNumPages{PLOS ONE}{10}{10}{1-22}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Ostrom%
}{%
Ostrom%
}{%
{\protect \APACyear {1990}}%
}]{%
Ostrom1990}
\APACinsertmetastar {%
Ostrom1990}%
\begin{APACrefauthors}%
Ostrom, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{1990}.
\newblock
\APACrefbtitle {Governing the commons: The evolution of institutions for
  collective action} {Governing the commons: The evolution of institutions for
  collective action}.
\newblock
\APACaddressPublisher{}{Cambridge university press}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Ostrom%
}{%
Ostrom%
}{%
{\protect \APACyear {1998}}%
}]{%
Ostrom1998}
\APACinsertmetastar {%
Ostrom1998}%
\begin{APACrefauthors}%
Ostrom, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1998}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A Behavioral Approach to the Rational Choice Theory of
  Collective Action: Presidential Address, American Political Science
  Association, 1997} {A behavioral approach to the rational choice theory of
  collective action: Presidential address, american political science
  association, 1997}.{\BBCQ}
\newblock
\APACjournalVolNumPages{American Political Science Review}{92}{1}{1-22}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pel{\'a}ez~Thompson%
\ \BBA {} V{\'a}squez~Duque%
}{%
Pel{\'a}ez~Thompson%
\ \BBA {} V{\'a}squez~Duque%
}{%
{\protect \APACyear {2018}}%
}]{%
pelaez2018evolucion}
\APACinsertmetastar {%
pelaez2018evolucion}%
\begin{APACrefauthors}%
Pel{\'a}ez~Thompson, S\BPBI M.%
\BCBT {}\ \BBA {} V{\'a}squez~Duque, A\BPBI M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2018}.
\unskip\
\newblock
\APACrefbtitle {Evoluci{\'o}n y causas de la pobreza en el departamento del
  {C}hoc{\'o} durante el siglo {XXI}} {Evoluci{\'o}n y causas de la pobreza en
  el departamento del {C}hoc{\'o} durante el siglo {XXI}}\
  \APACtypeAddressSchool {{B.S.} thesis}{}{}.
\unskip\
\newblock
\APACaddressSchool {}{Fundaci{\'o}n Universidad de Am{\'e}rica}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pe{\~n}a%
, V{\'e}lez%
, C{\'a}rdenas%
, Perdomo%
\BCBL {}\ \BBA {} Matajira%
}{%
Pe{\~n}a%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
Penaetal2017}
\APACinsertmetastar {%
Penaetal2017}%
\begin{APACrefauthors}%
Pe{\~n}a, X.%
, V{\'e}lez, M\BPBI A.%
, C{\'a}rdenas, J\BPBI C.%
, Perdomo, N.%
\BCBL {}\ \BBA {} Matajira, C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Collective Property Leads to Household Investments:
  Lessons from Land Titling in {Afro-Colombian} Communities} {Collective
  property leads to household investments: Lessons from land titling in
  {Afro-Colombian} communities}.{\BBCQ}
\newblock
\APACjournalVolNumPages{World Development}{97}{}{27--48}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pfaff%
, Robalino%
, Lima%
, Sandoval%
\BCBL {}\ \BBA {} Herrera%
}{%
Pfaff%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
Pfaff_etal2014}
\APACinsertmetastar {%
Pfaff_etal2014}%
\begin{APACrefauthors}%
Pfaff, A.%
, Robalino, J.%
, Lima, E.%
, Sandoval, C.%
\BCBL {}\ \BBA {} Herrera, L\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Governance, location and avoided deforestation from
  protected areas: greater restrictions can have lower impact, due to
  differences in location} {Governance, location and avoided deforestation from
  protected areas: greater restrictions can have lower impact, due to
  differences in location}.{\BBCQ}
\newblock
\APACjournalVolNumPages{World Development}{55}{}{7--20}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Poteete%
\ \BBA {} Ostrom%
}{%
Poteete%
\ \BBA {} Ostrom%
}{%
{\protect \APACyear {2004}}%
}]{%
PoteeteOstrom(2004)}
\APACinsertmetastar {%
PoteeteOstrom(2004)}%
\begin{APACrefauthors}%
Poteete, A\BPBI R.%
\BCBT {}\ \BBA {} Ostrom, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Heterogeneity, group size and collective action: the
  role of institutions in forest management} {Heterogeneity, group size and
  collective action: the role of institutions in forest management}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Development and change}{35}{3}{435--461}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Robalino%
\ \BBA {} Pfaff%
}{%
Robalino%
\ \BBA {} Pfaff%
}{%
{\protect \APACyear {2012}}%
}]{%
RobalinoPfaff(2012)}
\APACinsertmetastar {%
RobalinoPfaff(2012)}%
\begin{APACrefauthors}%
Robalino, J\BPBI A.%
\BCBT {}\ \BBA {} Pfaff, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2012}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Contagious development: Neighbor interactions in
  deforestation} {Contagious development: Neighbor interactions in
  deforestation}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Development Economics}{97}{2}{427--436}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Romero%
\ \BBA {} Saavedra%
}{%
Romero%
\ \BBA {} Saavedra%
}{%
{\protect \APACyear {2020}}%
}]{%
replication}
\APACinsertmetastar {%
replication}%
\begin{APACrefauthors}%
Romero, M.%
\BCBT {}\ \BBA {} Saavedra, S.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2020}{}{}.
\newblock
\APACrefbtitle {{Replication code for: Communal Property Rights and
  Deforestation}.} {{Replication code for: Communal Property Rights and
  Deforestation}.}
\newblock
\APACaddressPublisher{}{Harvard Dataverse}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.7910/DVN/YMWIAR} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.7910/DVN/YMWIAR} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Rueda%
}{%
Rueda%
}{%
{\protect \APACyear {2010}}%
}]{%
Rueda2010}
\APACinsertmetastar {%
Rueda2010}%
\begin{APACrefauthors}%
Rueda, X.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Understanding deforestation in the southern Yucat{\'a}n:
  insights from a sub-regional, multi-temporal analysis} {Understanding
  deforestation in the southern yucat{\'a}n: insights from a sub-regional,
  multi-temporal analysis}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Regional Environmental Change}{10}{3}{175--189}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Rustagi%
, Engel%
\BCBL {}\ \BBA {} Kosfeld%
}{%
Rustagi%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2010}}%
}]{%
rustagi2010conditional}
\APACinsertmetastar {%
rustagi2010conditional}%
\begin{APACrefauthors}%
Rustagi, D.%
, Engel, S.%
\BCBL {}\ \BBA {} Kosfeld, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Conditional cooperation and costly monitoring explain
  success in forest commons management} {Conditional cooperation and costly
  monitoring explain success in forest commons management}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Science}{330}{6006}{961--965}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Shin%
\ \protect \BOthers {.}}{%
Shin%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2020}}%
}]{%
SHIN2020106657}
\APACinsertmetastar {%
SHIN2020106657}%
\begin{APACrefauthors}%
Shin, H\BPBI C.%
, Yu, D\BPBI J.%
, Park, S.%
, Anderies, J\BPBI M.%
, Abbott, J\BPBI K.%
, Janssen, M\BPBI A.%
\BCBL {}\ \BBA {} Ahn, T.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2020}{}{}.
\newblock
{\BBOQ}\APACrefatitle {How do resource mobility and group size affect
  institutional arrangements for rule enforcement? A qualitative comparative
  analysis of fishing groups in {South Korea}} {How do resource mobility and
  group size affect institutional arrangements for rule enforcement? a
  qualitative comparative analysis of fishing groups in {South Korea}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Ecological Economics}{174}{}{106657}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
{Sistema de Informaci\'on Geogr\'afica para la planeaci\'on y el Ordenamiento
  Territorial ({SIGOT})}%
}{%
{Sistema de Informaci\'on Geogr\'afica para la planeaci\'on y el Ordenamiento
  Territorial ({SIGOT})}%
}{%
{\protect \APACyear {2019}}%
}]{%
sigot}
\APACinsertmetastar {%
sigot}%
\begin{APACrefauthors}%
{Sistema de Informaci\'on Geogr\'afica para la planeaci\'on y el Ordenamiento
  Territorial ({SIGOT})}.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2019}{}{}.
\newblock
\APACrefbtitle {Tierras de Comunidades Negras.} {Tierras de comunidades
  negras.}
\newblock
\begin{APACrefURL} [{01/06/2019}]\url{http://sigotvg.igac.gov.co:8080/}
  \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Velez%
, Robalino%
, Cardenas%
, Paz%
\BCBL {}\ \BBA {} Pacay%
}{%
Velez%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2020}}%
}]{%
Velez_etal2020}
\APACinsertmetastar {%
Velez_etal2020}%
\begin{APACrefauthors}%
Velez, M\BPBI A.%
, Robalino, J.%
, Cardenas, J\BPBI C.%
, Paz, A.%
\BCBL {}\ \BBA {} Pacay, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2020}{}{}.
\newblock
{\BBOQ}\APACrefatitle {{Is Collective Titling Enough to Protect Forests?
  Evidence from Afro-descendant Communities in the Colombian Pacific Region}}
  {{Is Collective Titling Enough to Protect Forests? Evidence from
  Afro-descendant Communities in the Colombian Pacific Region}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{World Development}{128}{}{104837}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Yang%
\ \protect \BOthers {.}}{%
Yang%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
Yang10916}
\APACinsertmetastar {%
Yang10916}%
\begin{APACrefauthors}%
Yang, W.%
, Liu, W.%
, Vi{\~n}a, A.%
, Tuanmu, M\BHBI N.%
, He, G.%
, Dietz, T.%
\BCBL {}\ \BBA {} Liu, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Nonlinear effects of group size on collective action and
  resource outcomes} {Nonlinear effects of group size on collective action and
  resource outcomes}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Proceedings of the National Academy of
  Sciences}{110}{27}{10916--10921}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}
