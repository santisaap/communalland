% Encoding: windows-1252

@Article{AGRAWAL20011649,
  author  = {Arun Agrawal},
  journal = {World Development},
  title   = {Common Property Institutions and Sustainable Governance of Resources},
  year    = {2001},
  number  = {10},
  pages   = {1649 - 1672},
  volume  = {29},
}

@Article{agrawal2001group,
  Title                    = {Group size and collective action: Third-party monitoring in common-pool resources},
  Author                   = {Agrawal, Arun and Goyal, Sanjeev},
  Journal                  = {Comparative Political Studies},
  Year                     = {2001},
  Number                   = {1},
  Pages                    = {63--93},
  Volume                   = {34},

  Publisher                = {Sage Publications Thousand Oaks}
}

@Article{Alix2007,
  Title                    = {A spatial analysis of common property deforestation},
  Author                   = {Alix-Garcia, Jennifer},
  Journal                  = {Journal of Environmental Economics and Management},
  Year                     = {2007},
  Number                   = {2},
  Pages                    = {141--157},
  Volume                   = {53},

  Publisher                = {Elsevier}
}

@Article{Alixetal2017,
  author  = {Alix-Garcia, Jennifer and Rausch, Lisa L. and L'Roe, Jessica and Gibbs, Holly K. and Munger, Jacob},
  journal = {Conservation Letters},
  title   = {Avoided Deforestation Linked to Environmental Registration of Properties in the {Brazilian Amazon}},
  year    = {2018},
  number  = {3},
  pages   = {e12414},
  volume  = {11},
}

@Article{Andam_etal2008,
  Title                    = {Measuring the effectiveness of protected area networks in reducing deforestation},
  Author                   = {Andam, Kwaw S and Ferraro, Paul J and Pfaff, Alexander and Sanchez-Azofeifa, G Arturo and Robalino, Juan A},
  Journal                  = {Proceedings of the national academy of sciences},
  Year                     = {2008},
  Number                   = {42},
  Pages                    = {16089--16094},
  Volume                   = {105},

  Publisher                = {National Acad Sciences}
}

@Article{barcelo2015group,
  Title                    = {Group size effect on cooperation in one-shot social dilemmas},
  Author                   = {Barcelo, H{\'e}lene and Capraro, Valerio},
  Journal                  = {Scientific reports},
  Year                     = {2015},
  Number                   = {1},
  Pages                    = {1--8},
  Volume                   = {5},

  Publisher                = {Nature Publishing Group}
}

@Article{Barsimantovetal2012,
  Title                    = {Community forestry, common property, and deforestation in eight {Mexican} states},
  Author                   = {Barsimantov, James and Kendall, Jake},
  Journal                  = {The Journal of Environment \& Development},
  Year                     = {2012},
  Number                   = {4},
  Pages                    = {414--437},
  Volume                   = {21},

  Publisher                = {SAGE Publications Sage CA: Los Angeles, CA}
}

@Article{BenYishay_etal2017,
  Title                    = {Indigenous land rights and deforestation: Evidence from the {Brazilian Amazon}},
  Author                   = {BenYishay, Ariel and Heuser, Silke and Runfola, Daniel and Trichler, Rachel},
  Journal                  = {Journal of Environmental Economics and Management},
  Year                     = {2017},
  Pages                    = {29--47},
  Volume                   = {86},

  Publisher                = {Elsevier}
}

@Article{Blackman_etal2017,
  Title                    = {Titling indigenous communities protects forests in the {Peruvian Amazon}},
  Author                   = {Blackman, Allen and Corral, Leonardo and Lima, Eirivelthon Santos and Asner, Gregory P},
  Journal                  = {Proceedings of the National Academy of Sciences},
  Year                     = {2017},
  Number                   = {16},
  Pages                    = {4123--4128},
  Volume                   = {114},

  Publisher                = {National Acad Sciences}
}

@Article{BlackmanVeit2018,
  Title                    = {Amazon Indigenous Communities Cut Forest Carbon Emissions},
  Author                   = {Blackman, Allen and Veit, Peter},
  Journal                  = {Ecological Economics},
  Year                     = {2018},
  Pages                    = {56--67},
  Volume                   = {153},

  Publisher                = {Elsevier}
}

@Article{BonillaHiguera2019,
  author  = {Leonardo Bonilla-Mejia and Iv\'an Higuera-Mendieta},
  journal = {World Development},
  title   = {Protected Areas under Weak Institutions: Evidence from {Colombia}},
  year    = {2019},
  pages   = {585 - 596},
  volume  = {122},
}

@TechReport{Burgessetal2016,
  Title                    = {Wilderness Conservation and the Reach of the State: Evidence from National Borders in the {Amazon}},
  Author                   = {Burgess, Robin and Costa, Francisco J.M. and Olken, Benjamin A},
  Institution              = {National Bureau of Economic Research},
  Year                     = {2018},
  Month                    = {July},
  Number                   = {24861},
  Type                     = {Working Paper},

  Doi                      = {10.3386/w24861},
  Series                   = {Working Paper Series},
  Url                      = {http://www.nber.org/papers/w24861}
}

@Article{Buschetal2017,
  Title                    = {What drives deforestation and what stops it? A meta-analysis},
  Author                   = {Busch, Jonah and Ferretti-Gallon, Kalifi},
  Journal                  = {Review of Environmental Economics and Policy},
  Year                     = {2017},
  Number                   = {1},
  Pages                    = {3--23},
  Volume                   = {11},

  Publisher                = {Oxford University Press}
}

@Article{Calonicoetal2017,
  Title                    = {rdrobust: Software for regression-discontinuity designs},
  Author                   = {Calonico, S. and M. D. Cattaneo and M. H. Farrell and R. Titiunik},
  Journal                  = {Stata Journal},
  Year                     = {2017},
  Number                   = {17},
  Pages                    = {372-404},

  Owner                    = {Mauricio},
  Timestamp                = {2017.12.15}
}

@TechReport{Chavez_etal2019,
  author      = {Carlos A. Ch\'avez and James J. Murphy and John K. Stranlund},
  institution = {Chapman University, Economic Science Institute},
  title       = {{Co-enforcement of Common Pool Resources: Experimental Evidence from TURFs in Chile}},
  year        = {2019},
  number      = {19-18},
  type        = {Working Papers},
  url         = {https://ideas.repec.org/p/chu/wpaper/19-18.html},
}

@Article{Chomitz1996,
  author  = {Kenneth M. Chomitz and David A. Gray},
  journal = {The World Bank Economic Review},
  title   = {Roads, Land Use, and Deforestation: A Spatial Model Applied to {B}elize},
  year    = {1996},
  number  = {3},
  pages   = {487--512},
  volume  = {10},
}

@Misc{Ley701993,
  author       = {{Congreso de Colombia}},
  howpublished = {Diario Oficial No. 41.013},
  title        = {Ley 70 de 1993},
  year         = {1993},
}

@Book{DasguptaHeal1979,
  author    = {Dasgupta, P.S. and Heal, G.M.},
  publisher = {Cambridge University Press},
  title     = {Economic Theory and Exhaustible Resources},
  year      = {1979},
  series    = {Cambridge Economic Handbooks},
  place     = {Cambridge, U.K.},
}

@Other{divagis,
  author  = {{DIVA-GIS}},
  title   = {Roads in {C}olombia},
  url     = {http://biogeo.ucdavis.edu/data/diva/rds/COL_rds.zip},
  urldate = {06/01/2019},
  year    = {2019},
}

@Other{El2016,
  Title                    = {As\'i viven en la {C}olombia a la que no se puede llegar por carretera},
  Author                   = {{El Tiempo}},
  Url                      = {https://www.eltiempo.com/colombia/otras-ciudades/colombia-pueblos-sin-vias-de-comunicacion-46383},
  Urldate                  = {19/12/2019},
  Year                     = {2016}
}

@Article{Ferraro_etal2014,
  Title                    = {Quantifying causal mechanisms to determine how protected areas affect poverty through changes in ecosystem services and infrastructure},
  Author                   = {Ferraro, Paul J and Hanauer, Merlin M},
  Journal                  = {Proceedings of the National Academy of Sciences},
  Year                     = {2014},
  Number                   = {11},
  Pages                    = {4332--4337},
  Volume                   = {111},

  Publisher                = {National Acad Sciences}
}

@TechReport{IDEAM2014,
  author      = {Galindo, G and Espejo, OJ and Rubiano, JC and Vergara, LK and Cabrera, E},
  institution = {Instituto de Hidrolog\'ia, Meteorolog\'ia y Estudios Ambientales --- IDEAM. Bogot\'a D.C., Colombia},
  title       = {Protocolo de procesamiento digital de im{\'a}genes para la cuantificaci{\'o}n de la deforestaci{\'o}n en {Colombia} {V}2.0},
  year        = {2014},
  type        = {techreport},
  url         = {http://www.ideam.gov.co/documents/11769/44688974/Protocolo+de+PDI+para+la+cuantificacion+de+la+deforestacion+en+colombia+v2_1_.pdf/00b95004-53dd-49f9-ab09-16d8803ccd92?version=1.0},
  urldate     = {01/06/2019},
}

@Book{Gilmour2016,
  author    = {Gilmour, Don},
  publisher = {Food and Agriculture Organization of the United Nations},
  title     = {Forty years of community-based forestry: A review of its extent and effectiveness},
  year      = {2016},
}

@TechReport{NBERw25018,
  author      = {Goodman-Bacon, Andrew},
  institution = {National Bureau of Economic Research},
  title       = {Difference-in-Differences with Variation in Treatment Timing},
  year        = {2018},
  number      = {25018},
  type        = {Working Paper},
  series      = {Working Paper Series},
  url         = {http://www.nber.org/papers/w25018},
}

@Article{Hansen_etal,
  author  = {Hansen, M. C. and Potapov, P. V. and Moore, R. and Hancher, M. and Turubanova, S. A. and Tyukavina, A. and Thau, D. and Stehman, S. V. and Goetz, S. J. and Loveland, T. R. and Kommareddy, A. and Egorov, A. and Chini, L. and Justice, C. O. and Townshend, J. R. G.},
  journal = {Science},
  title   = {High-Resolution Global Maps of 21st-Century Forest Cover Change},
  year    = {2013},
  number  = {6160},
  pages   = {850--853},
  volume  = {342},
}

@Article{Hayes2017,
  Title                    = {The Impact of Payments for Environmental Services on Communal Lands: An Analysis of the Factors Driving Household Land-Use Behavior in {Ecuador}},
  Author                   = {Tanya Hayes and Felipe Murtinho and Hendrik Wolff},
  Journal                  = {World Development},
  Year                     = {2017},
  Number                   = {Supplement C},
  Pages                    = {427 - 446},
  Volume                   = {93},

  Abstract                 = {Summary This article examines how Payments for Environmental Services (PES) influence household land-use behavior in the context of common-property lands. PES programs have been increasingly applied to communities who collectively manage their lands. While a number of authors have expressed concerns about the ability of said programs to generate additional environmental benefits and the potential for PES to counter community resource management arrangements, few empirical studies have explicitly examined PES in the context of communal resource management. Here, we take advantage of the gradual rollout of an Ecuadorian PES program to compare land-use behavior on collective lands in participant communities to households in communities that are waiting to participate. The goals of the analysis are to (a) identify if the PES program has produced changes in land-use, (b) assess the degree to which household characteristics and communal governance conditions drive land-use behavior, and (c) explore the interplay between PES and communal resource management institutions. Data were gathered from a cross-sectional survey of 399 households located in 11 communities. We use difference-in-differences to estimate the average effect of PES program participation on household behavior. Logit models, coupled with qualitative analysis, unpack how communal governance characteristics influence land-use behavior and the interplay between communal governance conditions and PES. We find that PES reduced the number of households grazing livestock on collective lands by 12%, however, household and communal governance factors are also instrumental in determining land-use decisions. Our results provide empirical insights into the debate over PES in collective resource management and illustrate how PES and communal resource management institutions can build upon each other to attain desired household conservation behavior.},
  Doi                      = {https://doi.org/10.1016/j.worlddev.2017.01.003},
  ISSN                     = {0305-750X},
  Keywords                 = {collective action, conservation, common-pool resource, ecosystem services, Latin America, páramo},
  Owner                    = {Mauricio},
  Timestamp                = {2017.12.14},
  Url                      = {http://www.sciencedirect.com/science/article/pii/S0305750X17300050}
}

@Other{IDEAM2011,
  Title                    = {Hoja metodol\'ogica del indicador Cambio en la superficie cubierta por bosque natural (Versi\'on 1,00)},
  Author                   = {Instituto de Hidrolog\'ia, Meteorolog\'ia y Estudios Ambientales - IDEAM},
  Note                     = {Sistema de Indicadores Ambientales de Colombia - Indicadores de Coberturas naturales de la tierra},
  Year                     = {2011}
}

@Other{ideam,
  author  = {{Instituto de Hidrolog\'ia, Meteorolog\'ia y Estudios Ambientales (IDEAM)}},
  title   = {Cobertura de Bosque No Bosque},
  url     = {http://www.ideam.gov.co/capas-geo},
  urldate = {01/06/2019},
  year    = {2019},
}

@Other{CIESIN2016,
  author      = {{Center for International Earth Science Information Network - CIESIN - Columbia University}},
  doi         = {http://dx.doi.org/10.7927/H4F47M2C},
  institution = {NASA Socioeconomic Data and Applications Center (SEDAC)},
  title       = {Gridded Population of the World, Version 4 (GPWv4): Administrative Unit Center Points with Population Estimates},
  year        = {2016},
}

@Book{DeJanvrySadoulet2001,
  author    = {De Janvry, Alain and Sadoulet, Elisabeth and Wolford, Wendy},
  publisher = {UNU World Institute for Development Economics Research Helsinki},
  title     = {Access to land and land policy reforms},
  year      = {2001},
  volume    = {3},
}

@Article{JonesLewis(2015),
  author    = {Jones, Kelly W. AND Lewis, David J.},
  journal   = {PLOS ONE},
  title     = {Estimating the Counterfactual Impact of Conservation Programs on Land Cover Outcomes: The Role of Matching and Panel Regression Techniques},
  year      = {2015},
  number    = {10},
  pages     = {1-22},
  volume    = {10},
  publisher = {Public Library of Science},
}

@Book{Molnar2004,
  Title                    = {Who Conserves the World's Forests? A New Assessment of Conservation and Investment Trend},
  Author                   = {Augusta Molnar and Sara J. Scherr and Arvind Khare},
  Publisher                = {Forest Trends},
  Year                     = {2004},

  Owner                    = {Mauricio},
  Timestamp                = {2017.12.15}
}

@Unpublished{montero2017cooperative,
  Title                    = {Cooperative Property Rights and Agricultural Development: Evidence from Land Reform in {El Salvador}},
  Author                   = {Montero, Eduardo},
  Year                     = {2017}
}

@Article{Ostrom1998,
  author    = {Ostrom, Elinor},
  journal   = {American Political Science Review},
  title     = {A Behavioral Approach to the Rational Choice Theory of Collective Action: Presidential Address, American Political Science Association, 1997},
  year      = {1998},
  number    = {1},
  pages     = {1-22},
  volume    = {92},
  publisher = {Cambridge University Press},
}

@Book{Ostrom1990,
  Title                    = {Governing the commons: The evolution of institutions for collective action},
  Author                   = {Ostrom, Elinor},
  Publisher                = {Cambridge university press},
  Year                     = {1990}
}

@Article{Penaetal2017,
  Title                    = {Collective Property Leads to Household Investments: Lessons from Land Titling in {Afro-Colombian} Communities},
  Author                   = {Pe{\~n}a, Ximena and V{\'e}lez, Mar{\'\i}a Alejandra and C{\'a}rdenas, Juan Camilo and Perdomo, Natalia and Matajira, Camilo},
  Journal                  = {World Development},
  Year                     = {2017},
  Pages                    = {27--48},
  Volume                   = {97},

  Publisher                = {Elsevier}
}

@MastersThesis{pelaez2018evolucion,
  author = {Pel{\'a}ez Thompson, Sonia Marcela and V{\'a}squez Duque, Ana Mar{\'\i}a},
  school = {Fundaci{\'o}n Universidad de Am{\'e}rica},
  title  = {Evoluci{\'o}n y causas de la pobreza en el departamento del {C}hoc{\'o} durante el siglo {XXI}},
  year   = {2018},
  type   = {{B.S.} thesis},
}

@Article{Pfaff_etal2014,
  Title                    = {Governance, location and avoided deforestation from protected areas: greater restrictions can have lower impact, due to differences in location},
  Author                   = {Pfaff, Alexander and Robalino, Juan and Lima, Eirivelthon and Sandoval, Catalina and Herrera, Luis Diego},
  Journal                  = {World Development},
  Year                     = {2014},
  Pages                    = {7--20},
  Volume                   = {55},

  Publisher                = {Elsevier}
}

@Article{PoteeteOstrom(2004),
  Title                    = {Heterogeneity, group size and collective action: the role of institutions in forest management},
  Author                   = {Poteete, Amy R and Ostrom, Elinor},
  Journal                  = {Development and change},
  Year                     = {2004},
  Number                   = {3},
  Pages                    = {435--461},
  Volume                   = {35},

  Publisher                = {Wiley Online Library}
}

@Article{RobalinoPfaff(2012),
  Title                    = {Contagious development: Neighbor interactions in deforestation},
  Author                   = {Robalino, Juan A and Pfaff, Alexander},
  Journal                  = {Journal of Development Economics},
  Year                     = {2012},
  Number                   = {2},
  Pages                    = {427--436},
  Volume                   = {97},

  Publisher                = {Elsevier}
}

@Article{Robinsonetal(2017),
  Title                    = {Community land titles alone will not protect forests},
  Author                   = {Robinson, Brian E and Holland, Margaret B and Naughton-Treves, Lisa},
  Journal                  = {Proceedings of the National Academy of Sciences},
  Year                     = {2017},
  Number                   = {29},
  Pages                    = {E5764--E5764},
  Volume                   = {114},

  Publisher                = {National Acad Sciences}
}

@Article{Rueda2010,
  Title                    = {Understanding deforestation in the southern Yucat{\'a}n: insights from a sub-regional, multi-temporal analysis},
  Author                   = {Rueda, Ximena},
  Journal                  = {Regional Environmental Change},
  Year                     = {2010},
  Number                   = {3},
  Pages                    = {175--189},
  Volume                   = {10},

  Publisher                = {Springer}
}

@Article{rustagi2010conditional,
  Title                    = {Conditional cooperation and costly monitoring explain success in forest commons management},
  Author                   = {Rustagi, Devesh and Engel, Stefanie and Kosfeld, Michael},
  Journal                  = {Science},
  Year                     = {2010},
  Number                   = {6006},
  Pages                    = {961--965},
  Volume                   = {330},

  Publisher                = {American Association for the Advancement of Science}
}

@Book{SeymourBusch2016,
  Title                    = {Why Forests? Why Now?: The Science, Economics, and Politics of Tropical Forests and Climate Change},
  Author                   = {Seymour, F. and Busch, J.},
  Publisher                = {Center for Global Development},
  Year                     = {2016},

  ISBN                     = {9781933286860},
  Lccn                     = {2016056035}
}

@Article{SHIN2020106657,
  author  = {Hoon C. Shin and David J. Yu and Samuel Park and John M. Anderies and Joshua K. Abbott and Marco A. Janssen and T.K. Ahn},
  journal = {Ecological Economics},
  title   = {How do resource mobility and group size affect institutional arrangements for rule enforcement? A qualitative comparative analysis of fishing groups in {South Korea}},
  year    = {2020},
  pages   = {106657},
  volume  = {174},
}

@Other{sigot,
  author  = {{Sistema de Informaci\'on Geogr\'afica para la planeaci\'on y el Ordenamiento Territorial ({SIGOT})}},
  title   = {Tierras de Comunidades Negras},
  url     = {http://sigotvg.igac.gov.co:8080/},
  urldate = {01/06/2019},
  year    = {2019},
}

@Article{Velez_etal2020,
  author  = {Maria Alejandra Velez and Juan Robalino and Juan Camilo Cardenas and Andrea Paz and Eduardo Pacay},
  journal = {World Development},
  title   = {{Is Collective Titling Enough to Protect Forests? Evidence from Afro-descendant Communities in the Colombian Pacific Region}},
  year    = {2020},
  pages   = {104837},
  volume  = {128},
}

@Article{Yang10916,
  author  = {Yang, Wu and Liu, Wei and Vi{\~n}a, Andr{\'e}s and Tuanmu, Mao-Ning and He, Guangming and Dietz, Thomas and Liu, Jianguo},
  journal = {Proceedings of the National Academy of Sciences},
  title   = {Nonlinear effects of group size on collective action and resource outcomes},
  year    = {2013},
  number  = {27},
  pages   = {10916--10921},
  volume  = {110},
}

@Data{replication,
  author    = {Romero, Mauricio and Saavedra, Santiago},
  doi       = {10.7910/DVN/YMWIAR},
  publisher = {Harvard Dataverse},
  title     = {{Replication code for: Communal Property Rights and Deforestation}},
  url       = {https://doi.org/10.7910/DVN/YMWIAR},
  version   = {DRAFT VERSION},
  year      = {2020},
}

@Comment{jabref-meta: databaseType:bibtex;}
