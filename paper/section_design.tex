\subsection{Background}

Although Afro-Colombian communities have inhabited the west coast of Colombia since the nineteenth century, they did not hold the title of the land they occupied until Law 70 of 1993 established their right to communal land titles.\footnote{Unlike Mexican \textit{ejidos}, there is no private property within communal titles in Colombia \cite{Alix2007}.} The first six titles were granted in December 1996 in the department of Choc\'{o}. By the beginning of 2017 there were 168 titles, encompassing a total of 55,000 $km^2$ (see Figure \ref{fig:CummulativeTitled}). According to the law, only vacant lots west of a specified line --- which does not correspond to any political--administrative boundary (shown in red in Figure \ref{fig:MapaPacifico}) --- are eligible for titling.
\\[0.4cm]
The area that can be titled is located along the Pacific Coast of four departments (Choc\'o, Valle del Cauca, Cauca and Nari\~no). The area is geographically isolated from the rest of the country --- with only two paved roads leading to the coast from the interior \cite{El2016} --- and has the highest poverty rate in the country \cite{pelaez2018evolucion}.
\\[0.4cm]
To request a communal title, community members must form a local council (``Consejo Comunitario'') to be in charge of ``delimiting and assigning areas within the adjudicated lands; ensure the conservation and protection of the rights of collective property; ensure the preservation of cultural identity; ensure the conservation of natural resources; choose the legal representative of the respective community as a legal entity; and act as friendly constituents in the internal conflicts that may be reconciled'' \cite{Ley701993}. Once the local council is formed, it must request the title from the central government by providing: (i) a detailed description of the land to be titled; (ii) an ethno-historic background of the community; (iii) a demographic description of the families within the title; and (iv) a description of the traditional means of production. After the request is received, the law requires the government to send a delegate to verify the information within 60 days. The government then has another 60 days to either grant or deny the title. Yet in practice the process has historically taken an average of two and a half years (see Table \ref{tab:sum_stat_titles} and Figure \ref{fig:histReqGranted} in the Online Supplemntary Materials). 

\begin{figure}[H]

\caption{Communal titles under Law 70 of 1993\label{fig:communal}}

\begin{subfigure}{0.49\textwidth}

\caption{Total area under communal titles by year\label{fig:CummulativeTitled}}

\begin{center}

\includegraphics[width=\textwidth]{figures/CummulativeTitled.pdf}

\end{center}

\end{subfigure}

\hfill

\begin{subfigure}{0.49\textwidth}

\caption{Communal titles in 2016\label{fig:MapaPacifico}}

\begin{center}

\includegraphics[width=\textwidth]{figures/MapaPacificoNew.png}

\end{center}

\end{subfigure}

\begin{minipage}{\textwidth}

\footnotesize{\emph{Notes:} Authors' calculations based on \cite{sigot} data. Figure \ref{fig:CummulativeTitled} shows the cumulative area under communal titles (in $km^2$ from 1993 to 2016). Figure \ref{fig:MapaPacifico} shows the allocation of communal titles as of 2016. Only land to the left of the red line is eligible for communal titling. }

\end{minipage}

\end{figure}

\subsection{Data}

We rely on two main data sources in our study. First, for forest coverage we use data from \citeA{Hansen_etal}, which is a yearly data set from 2001 to 2016 that classifies each $30 m \times 30 m$ pixel as either covered by forest or not. We aggregate the data to $90 m \times 90 m$ pixels for computational purposes. We also use forest coverage information from the Institute of Hydrology, Meteorology and Environmental Studies (IDEAM, its Spanish acronym) for 1990, 2000, 2005, and 2010 as a robustness check \cite{ideam}. The main difference between \citeA{ideam}'s and \citeA{Hansen_etal}'s data is that the former requires a minimum of 10 acres of continuous forest to label an area as forest land; thus small patches of forest are excluded (see Online Supplemntary Materials Section \ref{sec:ideam_data} for more details). 
\\[0.4cm]
According to \citeA{Hansen_etal}'s data, over 96\% of the area eligible for communal titling was covered by forest in 2000; by 2016, the forest area had declined to just over 90\% (see Figure \ref{fig:forest_zona}).

\begin{figure}[H]

\caption{Forest cover in area eligible for communal titling \label{fig:forest_zona}}

\begin{center}

\includegraphics[width=0.6\textwidth]{figures/ForestSIAC_vs_Hansen.pdf}

\end{center}

\begin{minipage}{\textwidth}

\footnotesize{\emph{Notes:} The black dotted line represents the proportion of land in the area eligible for communal titling that is covered by forest according to \citeA{Hansen_etal}. The red dashed line represents the land area within the communal titling eligible zone covered by forest according to \citeA{ideam}.}

\end{minipage}

\end{figure}

Our second data source is the \textit{Sistema de Informaci\'on Geogr\'afica para la planeaci\'on y el Ordenamiento Territorial}, which we use for information on the location and characteristics of communal titles. The data includes the year in which the collective title was granted, as well as its boundaries and number of inhabitants. We collect the date on which the request was first filed by coding it from the resolutions that granted each title. Table \ref{tab:sum_stat_titles} presents the summary statistics of the communal titles in our statistical analysis. The average communal title in our data was granted in 2002 and encompasses 335 $km^2$.

\begin{center}

\begin{table}[H]

\centering

\caption{Characteristics of communal titles\label{tab:sum_stat_titles}}

\begin{threeparttable}

\centering

\input{tables/sumstats_tc}

\begin{tablenotes}

{\footnotesize

\item \emph{Notes:} An observation is a communal title. Our statistical analysis does not include all 168 titled communities, as not all titles have a suitable control area to test the effect of titling. We also exclude two titles that had missing information on population. }

\end{tablenotes}

\end{threeparttable}

\end{table}

\end{center}

\subsection{Other data}

We associate each pixel in our data with the closest communal title and calculate the distance between the pixel and the title border. We then combine spatial data from two other sources to create a data set at the pixel level: We use slope data from the ALOS Global Digital Surface Model by JAXA (see \url{http://www.eorc.jaxa.jp/ALOS/en/aw3d30/index.htm} for more details), and roads and rivers data from \citeA{divagis}.

\section{Empirical strategy} \label{sec:id}

To identify the effect of communal titling on deforestation, we use a differences-in-discontinuities strategy that compares areas just inside and just outside the communal titles (the discontinuities) before and after the titles are granted (the differences). We estimate the following model at the pixel-by-year level:

\begin{equation}\label{eq:emp}
Y_{ict}=\beta_0 + \beta_1 After_{ct} \times Inner_{i} + f(Distance_{i},Inner_{i})+\alpha X_i +\gamma_{Inner_i,c}+\gamma_{ct} + \varepsilon_{ict},
\end{equation}

where $Y_{ict}$ is an indicator for whether pixel $i$, in the vicinity of communal title $c$, lacks forest coverage in year $t$. By vicinity we mean that the pixel is close to the boundary of $c$, regardless of whether it is outside or inside the titled area. $After_{ct}$ equals 1 if the communal title for $c$ has been granted by year $t$, and $Inner_{i}$ is an indicator equal to 1 if pixel $i$ is inside the communal title. $Distance_{i}$ is the distance from pixel $i$ to the boundary of $c$. Inside the communal title we set the distance as negative, and outside as positive. $f$ is a flexible polynomial that we allow to be different on each side of the border (and thus, also has $Inner_{i}$ as an argument). $X_i$ are pixel-level controls: distance to the nearest road, distance to the nearest river and slope. We also include communal-title-inner and communal-title-year fixed effects: $\gamma_{Inner_i,c}$ and $\gamma_{ct}$. Finally, $\varepsilon_{ict}$ is the error term (which we cluster at the community-year level). We employ the standard practice of choosing the bandwidth near the discontinuity following \citeA{Calonicoetal2017} for each community, and show that the results are robust to using either half or twice the optimal bandwidth. The coefficient of interest is $\beta_1$, which measures the effect of collective land titling on the probability that a pixel will lack forest coverage.
\\[0.4cm]
The strategy focuses on the land surrounding the borders of all communal titles, which usually follow natural boundaries, such as rivers. We use borders that are not next to other communal titles or to the ocean.\footnote{An alternative identification strategy would focus on the area surrounding the arbitrary line set by Law 70 of 1993. Since the boundary of the title along the line is plausibly exogenous, the only difference between land inside and outside the title is the title itself. While the underlying identification assumption is easier to meet in practice, very few titles have a boundary that coincides with the line (see Figure \ref{fig:MapaPacifico}), and hence any estimates from this identification strategy are too imprecise to be informative.} $\gamma_{Inner_ic}$ absorbs any difference between land just inside and just outside the title. For example, if the border of the title is determined by a river, $\gamma_{Inner_ic}$ absorbs any difference between the two shores of the river. $\gamma_{ct}$ absorbs any time variation (for each title). For example, if a new road is constructed that allows easier access to a title, these fixed effects capture any change in deforestation (either inside or outside the title). Thus, the identification relies on three assumptions. First, deforestation does not determine the decision to title or the specific borders. For example, if a community decides to title because deforestation is increasing, this would not violate the identification assumption ($\gamma_{ct}$ would capture this). However, if the community decides to impose a border because deforestation is rising inside (or outside) the border, this would violate the identification assumption. As mentioned above, since borders usually follow natural boundaries, this seems unlikely. The second assumption is that no other change besides the titling process takes place at the same time within the titled area. This is unlikely to be a threat to identification, as it would require a policy change that has both the same timing and the same geospatial attributes as titling.\footnote{In 1991 Colombia changed its constitution, which resulted in a series of major reforms over the next few years including Law 70 of 1993, described above. Many other national reforms also took place. For instance, Law 100 of 1993 introduced a universal health insurance scheme that increased the percentage of the insured population from 24\% to over 90\% within a few years. But most of these changes took place across the country and did not affect differentially titled areas.} The third assumption is that there are no spillovers from treated to untreated areas. The parallel-trends assumption would be violated if deforestation increased right outside the titled area due to displacement from inside the title. We discuss whether this assumption is plausible in more detail in Section \ref{sec:robust}. We do find some evidence of positive spillovers of reduced deforestation also outside the tile. Thus our main specification may be under-estimating the treatment effect of communal titling.
\\[0.4cm]
Figure \ref{fig:id_visual} provides a visual representation of the identification strategy using a sample communal title. It compares land just outside the title (A2, black-and-white crosshatch pattern) to that just inside the title (A1, solid light green), before and after the title is issued to the community.

\begin{figure}[H]

\caption{Visual representation of differences-in-discontinuities identification strategy \label{fig:id_visual}}

\begin{center}

\includegraphics[width=0.5\textwidth]{figures/ID_ejemplo2_updated.pdf}

\end{center}

\begin{minipage}{\textwidth}

\footnotesize{\emph{Notes:} Example of areas used in the regressions. The area deep inside the title is depicted in solid dark green (A0), and the part near the title border in light green (A1). The black-and-white crosshatch pattern denotes land located just outside the title area (A2). The border of the title is between A1 and A2. The yellow area with vertical lines represents area further outside the title (A3). The area in brown represents land that is even further away (A4). The main regression compares A1 and A2. Areas A0, A3 and A4 are not included in the main regression, but A3 and A4 are used to study spillovers.}

\end{minipage}

\end{figure}

To check the parallel-trend assumption and the dynamic effects of titling, we estimate a model that allows the effect of titling ($\beta_1$) to vary over time. Specifically, we estimate the following model:

\begin{equation}\label{eq:empES}
Y_{ict}=\beta_0 + \sum_{\tau=-5}^{\tau=5} \beta_{1\tau} Title_{\tau ct} \times Inner_{i} + f(Distance_{i})+\alpha X_i +\gamma_{Inner_i,c}+\gamma_{ct} + \varepsilon_{ict},
\end{equation}

where $Title_{\tau ct}$ equals 1 if there are $\tau$ years to/from titling in $c$ (and 0 otherwise). 

%See results on Figure \ref{fig:EvStu}.

As a robustness check, we include pixel fixed effects ($\gamma_{i}$) to control for observable and unobservable characteristics of each individual pixel that are constant over time. This specification is almost identical to Equation \ref{eq:emp}, but allows us to control for fixed properties of each pixel such as: soil quality, potential agricultural productivity and suitability for cattle ranching. We also add pixel characteristics interacted with time trends as additional controls. Specifically, we estimate:

\begin{equation}\label{eq:emp_pixFE}
Y_{ict}=\beta_0 + \beta_1 After_{ct} \times Inner_{i} + \sum_t \alpha_t Year_t X_i +\gamma_{i}+\gamma_{ct} + \varepsilon_{ict}. 
\end{equation}

Thus, $\beta_1$ above captures the difference (inside and outside each title) in changes in deforestation in pixels over time (before and after titling).\footnote{By contrast, $\beta_1$ in Equation \ref{eq:emp} captures changes in deforestation in the whole area inside and outside each title, before and after titling.}

\subsection{Data balance}

Table \ref{tab:balance} presents the mean characteristics of the pixels just outside (Column 1) and just inside (Column 2) a communal title. Our data points are far from roads and close to rivers.
\\[0.4cm]
Table \ref{tab:balance} also presents the difference between these pixels inside and outside communal titles (Column 3) as well as the discontinuity, if any, at the threshold (Column 4). Column 4 presents the estimate of $\beta$ from the following regression: 

\begin{equation}\label{eq:RD}
Y_{ic}=\alpha + \beta Inner_{i} + \lambda Inner_{i} \times Distance_i + \delta Distance_i +\gamma_{c} + \varepsilon_{ic},
\end{equation}

where $Y_{ic}$ is the outcome of pixel $i$ in the vicinity of communal title $c$, and everything else is as in Equation \ref{eq:emp}. The results are robust to either using half or twice the optimal bandwidth (see Table \ref{tab:balance_extra} in the Online Supplemntary Materials).
 \\[0.4cm] 
As expected, pixels just inside and just outside the communal title are similar along several time-invariant characteristics, such as distance to the nearest road, distance to the nearest river, and slope (see Table \ref{tab:balance}, Column 4). 
\\[0.4cm]
Finally, Table \ref{tab:balance} also presents forest coverage in the land just outside and just inside the title. Before titling, the probability that a pixel was not covered by forest was (\input{tables/DefoOutBefore}\unskip\% inside and \input{tables/DefoInBefore}\unskip\% outside). At the border, the difference was 0.86 percentage points (which is statistically significant at the 1\% level). This suggests that a regression discontinuity design (such as the one used by \citeA{BonillaHiguera2019}) might yield biased estimates. After titling, the probability that a pixel was not covered by forest grew more outside the title than inside (to \input{tables/DefoOutAfter}\unskip\% outside and \input{tables/DefoInAfter}\unskip\% inside). 

\begin{center}

\begin{table}[H]

\centering

\caption{Summary statistics around the boundary of forest coverage and time-invariant covariates \label{tab:balance}}

\begin{threeparttable}

\centering

\begin{tabular}{l*{1}{cccc}}

\toprule

& (1) & (2) & (3) & (4) \\

& Outside & Inside & Difference &Discontinuity \\

& & & & \\

\input{tables/BalanceDeforest_1}

\input{tables/Balance_1}

\bottomrule

\end{tabular}

\begin{tablenotes}

\footnotesize

\item \emph{Notes:} This table presents the mean and standard error of the mean (in parentheses) for the pixel just outside (``Outside'' Column 1) and just inside the communal titles (``Inside'', Column 2). The last columns present the difference between inside and outside areas (Column 3), and the discontinuity at the threshold allows for a different linear fit inside and outside the title (Column 4). \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\)

\end{tablenotes}

\end{threeparttable}

\end{table}

\end{center}