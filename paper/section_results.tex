 

\subsection{Main results}

In this section, we explore how communal land titling affects forest coverage. We find that communal titling reduces the likelihood that a pixel lacks forest coverage (i.e., it reduces deforestation). The reduction is larger for smaller communities. This result is robust to different bandwidths, polynomial specifications, and measures of forest coverage. Replication data and code is available at \citeA{replication}
\\[0.4cm]
Figure \ref{fig:EvStu} displays the evolution of forest coverage inside and outside the title before and after titling (i.e., an event study from estimating Equation \ref{eq:empES}). Before titling, there are no significant differences between the probability that a pixel lacks forest coverage inside and outside the titled area. After titling, the probability that a pixel lacks forest coverage is lower inside the communal title, especially after the first year. There is a slight pre-trend that could be explained by the average lag of 2.5 years between filing the request for titling and the title being granted (see Table \ref{tab:sum_stat_titles} and Figure \ref{fig:histReqGranted} in the Online Supplemntary Materials).

\begin{figure}[H]

\caption{Lack of forest coverage inside and outside communal lands, before and after titling} \label{fig:EvStu}

\centering

\includegraphics[width=0.8\textwidth]{figures/EventStudy_All.pdf}

\begin{figurenotes}

\footnotesize{\emph{Notes:} This figure illustrates the event study of the effect of titling on the probability a pixel lacks forest coverage. Years to titling are on the x-axis (0 = the year of titling). The effect of titling on the probability a pixel lacks forest coverage is on the y-axis. Each point represents the coefficient from years to titling interacted with the treatment indicator (i.e., whether the pixel is inside the title). See Equation \ref{eq:empES}.}

\end{figurenotes}

\end{figure}

Next, we present the estimates from the differences-in-discontinuities (i.e., Equation \ref{eq:emp}; see Table \ref{tab:main}). Communal titling led to a decrease of \input{tables/coef_main1} (p-value$<$0.01) percentage points in the probability that a pixel lacks forest coverage from a mean of 4.95\% (a 6.7\% decrease). Since the total titled area is roughly 55,000 $km^2$, this means communal titling decreased deforestation by around 181 $km^2$. There is important heterogeneity in the number of inhabitants of the communal land.\footnote{Titles above and below the median in population have similar titling dates. See Tables \ref{tab:sum_stat_titles_pop} and \ref{tab:sum_stat_titles_area} in the Online Supplemntary Materials for details. The results are similar if area, instead of population, is used to estimate heterogeneity. Table \ref{taba:PopArea_Combined} provides more details.} 
\\[0.4cm]
The probability that a pixel lacks forest coverage decreases in small communities (below the median) by 0.53 percentage points, a drop of 10.7\% (Column 2). In large communities (above the median size) the probability decreases by 0.28 percentage points (a 5.7\% decrease). We also investigate the effects by terciles of population (Column 3): the largest effect is for communities in the smallest tercile of population. Imposing a parametric linear functional form, we confirm that the probability that a pixel lacks forest coverage decreases the most in small communities (Column 4). Finally, a quadratic functional (see Column 5) confirms that the titling effect is larger for smaller communities, although the coefficients are not statistically significant. 

\begin{table}[H]

\centering

\caption{Effect of communal titling on forest coverage \label{tab:main}}

\begin{threeparttable}

\centering

\begin{tabular}{l*{5}{c}}

\toprule

Dependent Variable: & \multicolumn{4}{c}{No forest coverage} \\

& (1) & (2) & (3) & (4) & (5)\\

\midrule

\addlinespace

\input{tables/pixeldis_fquad_reg_tabla_main_pixel}

\bottomrule

\end{tabular}

\begin{tablenotes}

\item \footnotesize{\emph{Notes:} The outcome variable is equal to 0 if the pixel is covered by forest, and 100 if it is not. Thus, the coefficients can be interpreted as the effect, in percentage points, on the probability a pixel is not covered by forest. \citeA{Hansen_etal} yearly data (2001--2016). Population (Pop) measured in 10,000 inhabitants. All regressions include communal-title-inner and communal-title-year fixed effects. Controls include distance to the nearest road, distance to the nearest river, and slope. Standard errors, clustered by community-year, are in parentheses. Optimal bandwidth is calculated for each community following \citeA{Calonicoetal2017}. \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\)}

\end{tablenotes}

\end{threeparttable}

\end{table}

\subsection{Robustness checks}\label{sec:robust}

The results are robust to using pixel fixed effects (i.e., estimating Equation \ref{eq:emp_pixFE}). The results  using \citeA{Hansen_etal}'s yearly data (Table \ref{tab:main_pix}, Columns 1 and 2) and pixel fixed effects are similar to those reported in Table \ref{tab:main}. The results are qualitatively similar using IDEAM data (Columns 3 and 4), which indicate a 0.87-percentage-point decrease from a base of 15.4\% in the probability that a pixel lacks forest coverage (p-value$<$0.01), which represents a 5.6\% decrease. The larger coefficient can be partially explained by the periodicity of the data (5-year intervals for \citeA{ideam} vs. yearly data for \citeA{Hansen_etal}). The larger standard errors are due to a smaller sample size.

\begin{table}[H]

\centering

\caption{Effect of communal titling on forest coverage\label{tab:main_pix}}

\begin{threeparttable}

\centering

\begin{tabular}{l*{4}{c}}

\toprule

Dependent Variable: & \multicolumn{4}{c}{No forest coverage} \\

& \multicolumn{2}{c}{Hansen} & \multicolumn{2}{c}{IDEAM} \\

& (1) & (2) & (3) & (4) \\

\midrule

\addlinespace

\input{tables/pixeldis_fquad_reg_tabla_main_pixel_pix}

\bottomrule

\end{tabular}

\begin{tablenotes}

\item \footnotesize{\emph{Notes:}  The outcome variable is equal to 0 if the pixel is covered by forest, and 100 if it is not. Thus, the coefficients can be interpreted as the effect, in percentage points, on the probability a pixel is not covered by forest. The table reports the results of estimating Equation \ref{eq:emp_pixFE}. All regressions include pixel fixed effects and time dummies interacted with distance to the nearest road, nearest river and slope. Columns 1 and 2 use \citeA{Hansen_etal} yearly data (2001--2016). Columns 3 and 4 use \citeA{ideam} data for 1990, 2000, 2005, and 2010. Standard errors, clustered by community-year, are in parentheses. Optimal bandwidth calculated for each community following \citeA{Calonicoetal2017}. \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\)}

\end{tablenotes}

\end{threeparttable}

\end{table}

Our results are also robust to at least two specification choices. First, the results do not vary qualitatively depending on the choice of optimal bandwidth (see Columns 1--3 of Table \ref{taba:robust_reg} in the Online Supplemntary Materials). Using a bandwidth that is twice as large or half as large yields similar results. Second, the results are unaffected by using linear instead of quadratic polynomials on the distance to the border (see Columns 4--6 of Table \ref{taba:robust_reg}).

\subsection{Spillovers}\label{sec:spillovers}

We perform additional regressions, varying the control group, to study spillovers (see Table \ref{tab:PlOc}). Column 1 replicates the results in the first column of Table \ref{tab:main}, where we compare the land just inside versus just outside the communal title (area A1 versus area A2 in Figure \ref{fig:id_visual}). The control group in this specification is the land that is within the optimal bandwidth from the border of the title and outside the title. As we move away from the border, we expect spillovers to fade out. Column 2 thus compares A1 with A3---that is, land just inside the title with land outside the title but farther from the border (i.e., an outer ring). The control group here is the land between the bandwidth and twice the bandwidth from the border. Column 3 performs a similar exercise but using a control group that is between two and three times the bandwidth from the border (i.e., it compares A1 to A4, Figure \ref{fig:id_visual}). The effect of titling increases as we use control groups that are farther from the border. We interpret this as evidence of positive spillovers. Thus, the results in Table \ref{tab:main} may be attenuated, and the total treatment effect of communal titling on deforestation may be larger.
\\[0.4cm]
Positive spillovers are further supported by Columns 4 and 5, which report the results of placebo exercises. In Column 4 we compare land just outside the border with land that is twice the bandwidth from the border (at most the optimal bandwidth away, or A2 vs. A3).  The probability a pixel lacks forest coverage decreases in land close to the title, as in \citeA{Chomitz1996}. We take this as further evidence of positive spillovers. Finally, in Column 5 we compare land outside the title that is twice the bandwidth away from the border with land that is three times the bandwidth away from the border (i.e., comparing A3 with A4). In this last specification we find no evidence of differential forest coverage, which can be explained by positive spillovers fading out with distance. 

\begin{table}[H]

\centering

\caption{Possible spillovers of titling \label{tab:PlOc}}

\begin{threeparttable}

\centering

\begin{tabular}{l*{5}{c}}

\toprule

Dependent Variable & \multicolumn{5}{c}{No forest coverage} \\

& (1) & (2) & (3) & (4) & (5) \\

& A1 vs A2 & A1 vs A3 & A1 vs A4 & A2 vs A3 & A3 vs A4 \\

\midrule

\addlinespace

\input{tables/pixeldis_fquad_reg_PlOc}

\bottomrule

\end{tabular}

\begin{tablenotes}

\item \footnotesize{\emph{Notes:}  The outcome variable equals 0 if the pixel is covered by forest, and 100 if it is not, using \citeA{Hansen_etal} yearly data (2001--2016). Thus, the coefficients can be interpreted as the effect, in percentage points, on the probability a pixel is not covered by forest. Each column compares different regions as defined in Figure \ref{fig:id_visual}. Controls include distance to the nearest road, distance to the nearest river, and slope. Standard errors, clustered by community-year, are in parentheses. Optimal bandwidth calculated for each community following \citeA{Calonicoetal2017}. \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\)}

\end{tablenotes}

\end{threeparttable}

\end{table}